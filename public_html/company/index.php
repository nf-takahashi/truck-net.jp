<?php
require_once $_SERVER['DOCUMENT_ROOT']. "/settings/settings.php";
$pageTitle = "会社案内";

$content=<<<HERE
<div class="breadclumb">
<ul itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
    <li><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
        <li><a href="/trucks/index.php">中古トラック</a></li>
    <li><span itemprop='title'>$pageTitle</li>
</ul>
</div>
<article class="company">
      <div class="wrap">
        <h1 class="title">会社案内</h1>
        <div class="main_text">
          <p>私たちワンプラストアは栗山自動車グループの一員として誕生しました。</p>
          <p>業界で初めてトラック販売に保証制度を導入した栗山自動車に対して、<br>「とにかくとにかくとにかく安くして！」というお客様のご要望に応えるため、<br>徹底したコストカットを行っております。</p>
          <p>そのため、ローン審査・車検取得・整備・お取り置きなどのサービスは省かせて頂いております。<br>ご購入後、お客様が必要な箇所のみ整備工場様などでお手入れしていただき、稼働させていただくことを前提としております。</p>
          <p>価格については仕入後もお客様に喜んで頂けるよう、毎日市場調査し見直しております。</p>
          <p>精一杯お安くお届けするため、まるでボクシング選手のようなギリギリのコストカットしながらも、<br>出来るだけ良いお車を、もうひと頑張りしてもらえる稼げるトラックを厳選して仕入れております。<br>ただし入庫時の点検は目視のみと範囲は限られているため、お客様による実車確認をお勧め致します。</p>
          <p>ダメなものはダメ、それは小売りしない。<br>たとえ私たちが稼げても、お客様が稼げなきゃ販売する意味がない。</p>
          <p>車は安くても、誇りは高く、プロの商売を心がけ、<br>世界一安く、どこよりも稼げるトラックをお届けする事を使命とし精進してまいります。</p>
        </div>
        <div class="inner_link">
          <h2 class="title_sub">ワンプラストアについて</h2>
        </div>
        <div class="table">
          <dl>
              <dt>屋号</dt>
              <dd>ワンプラストア</dd>
          </dl>
          <dl>
              <dt>所在地</dt>
              <dd>〒134-0015　東京都江戸川区西瑞江5−3−10</dd>
          </dl>
          <dl>
              <dt>展示場</dt>
              <dd>
              〒285-0802　千葉県佐倉市大作2-12-2　<a href="#chibaMap">(地図)</a><br>
              <a href="tel:0434985170">043-498-5170(代)</a><br>
              定休日：日曜
            </dd>
          </dl>
        </div>
        <div class="inner_link" id="navi1">
          <h2 class="title_sub">運営会社について</h2>
        </div>
        <div class="table">
          <dl>
              <dt>運営会社</dt>
              <dd>栗山自動車工業株式会社</dd>
          </dl>
          <dl>
              <dt>代表者</dt>
              <dd>栗山義広</dd>
          </dl>
          <dl>
              <dt>TEL／FAX</dt>
              <dd>03-3689-7711／03-3689-7828</dd>
          </dl>
          <dl>
              <dt>営業時間</dt>
              <dd>
                月曜日～土曜日 9:00～18:00<br>
              </dd>
          </dl>
          <dl>
              <dt>休業日</dt>
              <dd>
                 日曜日・祝日・年末年始
              </dd>
          </dl>
          <dl>
              <dt>古物商許可番号</dt>
              <dd>東京都公安委員会　 第307790808899号</dd>
          </dl>
        </div>
        <div class="inner_link" id="navi2">
          <h2 class="title_sub">事業内容</h2>
        </div>
        <ul>
            <li>トラック中古車の販売</li>
            <li>トラック買取り事業</li>
        </ul>
        <h2 class="title_sub">グループ会社</h2>
        <div class="text">
            <a href="https://www.kuriyama-truck.com/" target="_blank">栗山自動車工業株式会社</a><br>
            <a href="https://www.truskey.jp/" target="_blank">株式会社トラスキー</a><br>
            <a href="http://www.truckcom.co.jp/" target="_blank">トラックコム</a><br>
        </div>
        <div class="inner_link" id="navi4">
          <h2 class="title_sub">アクセスマップ</h2>
        </div>
        <div class="text" id="chibaMap">
          <h3 class="text_title">千葉展示場</h3>
          <p>〒285-0063 千葉県佐倉市宮本110-1</p>
          <br>
          <p>東京駅 ～ 佐倉駅（JR線）で50分。駅からタクシーで20分。<br>
          <div class="googleMap">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3240.8669406714903!2d140.24369961525872!3d35.6802781801945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60228e69c4784bdd%3A0xedc3bd9be71571e1!2z44CSMjg1LTAwNjMg5Y2D6JGJ55yM5L2Q5YCJ5biC5a6u5pys77yR77yR77yQ4oiS77yR!5e0!3m2!1sja!2sjp!4v1533090281898" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
              <br>
              <span class="big_map">
                  <a href="https://goo.gl/maps/oA6NdkzP8Rm" target="_blank">大きな地図で見る</a>
              </span>
          </div><!-- /#googleMap -->
        </div>
      </div>
</article>
HERE;
HtmlSource::Output($content,$pageTitle); // HTML出力
?>
