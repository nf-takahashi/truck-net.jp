<?php
require_once $_SERVER['DOCUMENT_ROOT']. "/settings/settings.php";
$pageTitle = "特商法に基づく表示";

$content=<<<HERE
<div class="breadclumb">
<ul itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
    <li><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
        <li><a href="/trucks/index.php">中古トラック</a></li>
    <li><span itemprop='title'>$pageTitle</li>
</ul>
</div>
<article class="law">
        <div class="wrap">
            <h1 class="title">特商法に基づく表示</h1>

            <table>
              <tbody><tr>
                  <th>販売業者</th>
                  <td>栗山自動車工業株式会社</td>
              </tr>
              <tr>
                  <th>運営統括責任者</th>
                  <td>栗山義広</td>
              </tr>
              <tr>
                  <th>所在地</th>
                  <td>〒134-0015　東京都江戸川区西瑞江5−3−10</td>
              </tr>
              <tr>
                  <th>電話番号</th>
                  <td>03-3689-7711</td>
              </tr>
              <tr>
                  <th>FAX番号</th>
                  <td>03-3689-7828</td>
              </tr>
              <tr>
                  <th>ご注文方法</th>
                  <td>ご来店、ホームページ、電子メール、電話、ファックス、お手紙等</td>
              </tr>
              <tr>
                  <th>お見積の有効期限</th>
                  <td>お見積書は作成しておりません。</td>
              </tr>
              <tr>
                  <th>商品以外の必要料金</th>
                  <td>
                      陸送費…来店受取以外で陸送手配を希望される場合。<br>金融機関振込手数料…お客様による銀行または郵便局振込の場合。
                  </td>
              </tr>
              <tr>
                  <th>お引渡し方法</th>
                  <td>弊社千葉展示場にてお引渡し。（陸送会社手配の場合は別料金とさせていただきます。）</td>
              </tr>
              <tr>
                  <th>発送時期</th>
                  <td>お支払い後に発送いたします。</td>
              </tr>
              <tr>
                  <th>お車の確認</th>
                  <td>当社に御来店での現車確認及び公開情報メールでご確認して頂いております。</td>
              </tr>
              <tr>
                  <th>お支払い方法</th>
                  <td>
                      現金、銀行振り込み
                  </td>
              </tr>
              <tr>
                  <th>お支払い期限</th>
                  <td>ご契約後から納車前日までに当社指定口座へのお振り込み。または納車日に現金払い。</td>
              </tr>
              <tr>
                  <th>返品について</th>
                  <td>受け付けておりません。</td>
              </tr>
              <tr>
                  <th>返品の場合の送料</th>
                  <td>受け付けておりません。</td>
              </tr>
            </tbody>
          </table>
        </div>
</article>
HERE;
HtmlSource::Output($content,$pageTitle); // HTML出力
?>
