(function($) {
  $(function() {
    $(document)
      // Open Spec on detail page.
      .on('click', '.optional_spec', function () {
        $(this).addClass('on');
      // Toggle detail search menu on top page.
      })
      .on('click', '.search_title', function () {
        $(this).parent('.search_wrap').toggleClass('open');
      })
      // open modal -> bigger image
      .on('click', '.detil_top_image img', function () {
        console.log('click');
        if( $(window).width() > 767 )
        {
          var index = $('.detil_top_image img').index(this);
          $('body').toggleClass('modal_bigimage_on');
          var slick_overlay = $('[dom=image_big_overlay_slick]').slick({
            lazyLoad: 'ondemand',
            dots: true,
            nextArrow: '<button type="button" class="slick-next"></button>',
            prevArrow: '<button type="button" class="slick-prev"></button>'
          })
          slick_overlay.slick('slickGoTo', parseInt(index), true);
        }
      })
      .on('click', '.detail_images li', function () {
        if( $(window).width() > 767 )
        {
          var index = $('.detail_images li').index(this);
          $('body').toggleClass('modal_bigimage_on');
          var slick_overlay = $('[dom=image_big_overlay_slick]').slick({
            lazyLoad: 'ondemand',
            dots: true,
            nextArrow: '<button type="button" class="slick-next"></button>',
            prevArrow: '<button type="button" class="slick-prev"></button>'
          })
          slick_overlay.slick('slickGoTo', parseInt(index), true);
        }
      })
      .on('click', '.image_big_overlay_wrap img, [dom=close_modal]', function(){
        $('[dom=image_big_overlay_slick]').slick('unslick');
        $('body').toggleClass('modal_bigimage_on');
      })
      // Burger Menu
      .on('click', '#burger', function() {
          $('#nav').addClass('nav_on');
      })
      .on('click', '#close, [dom=buger_nav_base], .banner_kyusha_sp', function() {
          $('#nav').removeClass('nav_on');
      })
      // Toggle button for List page on mobile
      .on('click', '[dom=toggle_filter]', function() {
        $('body').toggleClass('show_filter');
        $('body').removeClass('show_filter_default');
        if( $('body').hasClass('show_filter') ){
          $.cookie('filter_window', 'on');
        }else{
          $.cookie('filter_window', 'off');
        }
      })
      .on('click', '.filter a', function(){
          $.cookie('filter_window', 'on');
      })
      .on('click', '[dom=btn_seach], #submit', function(){
          $.cookie('filter_window', 'off');
      })
      // Toggle Contact
      .on('click', '[dom=toggle_contact_sp],[dom=close_contact_sp]', function() {
        $('body').toggleClass('show_contact');
      })
			// SP
			.on('click', '.nav_main > li',function() {
				if( $(window).width() < 768)
				{
					if( $(this).hasClass('on') ){
						$('.nav_main > li').removeClass('on');
					}
					else{
						$('.nav_main > li').removeClass('on');
						$(this).addClass('on');
					}
				}
			})
      // popup link image
			.on('click', '[dom=popup]',function() {
        $('body').toggleClass('show_modal');
      })
      .on('click', '[dom=modal]',function() {
        $('body').removeClass('show_modal show_modal_add_line');
      })
      .on('click', '[dom=show_modal_line]',function() {
        $('body').toggleClass('show_modal');
        $('body').toggleClass('show_modal_add_line');
      })
			.on('click', '[dom=modal_inner]',function(e) {
        e.stopPropagation();
      })
      .on('click', '[dom=mail]',function() {
        function converter(M){
          var str="", str_as="";
          for(var i=0;i<M.length;i++){
            str_as = M.charCodeAt(i);
            str += String.fromCharCode(str_as + 1);
          }
          return str;
        }
        function mail_to(k_1,k_2){
          eval(String.fromCharCode(108,111,99,97,116,105,111,110,46,104,114,101,102,32,61,32,39,109,97,105,108,116,111,58) + escape(k_1) + converter(String.fromCharCode(106,110,114,96,99,96,63,106,116,113,104,120,96,108,96,44,115,113,116,98,106,45,98,110,108,62,114,116,97,105,100,98,115,60)) + escape(k_2) + "'");
        }
        mail_to("","");
      })
		;

    $('.product_list .sort select').change(function() {
        return $(this).closest('form').submit();
    });


    // Contact Button Loader
    var isSubmit = false;
    $('#form_contact').on('submit', (function() {
      if(isSubmit){
          // 既に送信されている場合
          return false;
        }else{
          $('.send').addClass('sending');
          isSubmit = true;
          return true;
        }
    }));

    $(window).ready(function(){
      // 参照元が同じドメインなら、スクロール
      console.log('referer');
      if( location.pathname != '/' && document.referrer.indexOf('truck-net.jp') != -1 ) {
        var refOffset, headerHeight, windowWidth, detailNavHeight;
        windowWidth = $(window).width();

        if( windowWidth > 768){
          headerHeight = 59;
        }else{
          headerHeight = 35;
        }

        refOffset = windowWidth * 0.5622807 + headerHeight;
        if( refOffset > 705) refOffset = 705;

        if( location.pathname.indexOf('detail.php') != -1){
          if( windowWidth > 768){
            detailNavHeight = 276;
          }else{
            detailNavHeight = 250;
          }


        }else{
          detailNavHeight = 0;
        }
        refOffset += detailNavHeight;

        $('body,html').animate({scrollTop:refOffset}, 400, 'swing');
      }
    });


  });
})(jQuery);
