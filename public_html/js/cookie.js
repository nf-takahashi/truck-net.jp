$(function(){
    // cookieの登録処理
    setCookie = function(key, value){
        // 既に登録されていれば削除する
        deleteCookie(key, value);

        var name=String(key);
        var str=value.toString();
        var buf_array=new Array(20);
        var no_array=new  Array();
        var chk_array=new  Array();
        var date=new Date(jQuery.now());
        var buf=value+";"+ dateToLocaleString(date);
        if($.cookie(name)){
            // クッキーが存在する場合
            buf_array=$.cookie(name).split(",");
            // 自社管理番号のみを配列化
            $.each(buf_array,function(key,value){
                line=value.split(";");
                chk_array.unshift(line[0]);
            });

            buf_array.unshift(buf);
            $.cookie(name,buf_array.join(","), {path:"/",expires:7000});

            return true;
        }else{
            // クッキーが存在しない場合
            $.cookie(name,buf,{path:"/",expires:7000});
            return true;
        }
    }

    // cookieの削除処理
    deleteCookie = function(key, value){
        var name=key.toString();
        var str=value.toString();
        var buf_array=new Array(20);
        var no_array=new  Array();
        var chk_array=new  Array();
        var date=new Date(jQuery.now());
        var buf=value+";"+ dateToLocaleString(date);
        if ($.cookie(name)){
            // クッキーが存在する場合
            buf_array=$.cookie(name).split(",");
            // 自社管理番号のみを配列化
            $.each(buf_array,function(key,value){
                var line=value.split(";");
                chk_array.unshift(line[0]);
            });
            // 該当自社管理番号が既にcookieへセットされていた場合は削除する
            if ($.inArray(str,chk_array) != -1) {
                for (var i=0; i<buf_array.length; i++){
                    if (buf_array[i].indexOf(str) != -1) {
                        buf_array.splice(i,1);
                    }
                }
                $.cookie(name,buf_array.join(","), {path:"/",expires:7000});
                return true;
            }else{
                // 削除されなかった場合
                return false;
            }
        }
    }
    // 検討リストに登録
    set_favorite = function(company_control_no, stock_no){

        var result = setCookie('truskey_favorite', company_control_no);
        if(result){
            // 成功
            alert(stock_no+"を検討リストに追加しました");
            location.reload();
        }else{
            alert(stock_no+"は登録済みです");
        }
    }

    set_favorite_no_reload = function (company_control_no, stock_no, element) {
        if ($(element).hasClass("stocked")) {
            location.href = '/favorite';
        } else {
            setCookie('truskey_favorite', company_control_no);
            $(element).find('span').html('検討リストを見る');
            $(element).addClass('stocked');
        }
    }

    saveLayout = function (layout){
        $.cookie("layout", null, { path: '/' });
        setCookie('layout', layout);
    }

    // 検討リスト削除
    delete_favorite = function(company_control_no,stock_no){
        var result = deleteCookie('truskey_favorite', company_control_no);
        if(result){
            // 成功
            alert(stock_no+"を検討リストから削除しました");
            location.reload();
        }else{
            alert(stock_no+"は検討リストに存在していません");
        }
    }

     // 検討リストに登録
    set_parts_favorite = function(parts_control_no, stock_no){
        var result = setCookie('kuriyama_parts_favorite', parts_control_no);

        if(result){
            // 成功
            if(confirm(stock_no+"を検討リストに追加しました")){
                location.reload();
            }
        }else{
            if(confirm(stock_no+"は登録済みです")){
                location.reload();
            }
        }
    }

    // 検討リスト削除
    delete_parts_favorite = function(company_control_no,stock_no){
        var result = deleteCookie('kuriyama_parts_favorite', company_control_no);

        if(result){
            // 成功
            if(confirm(stock_no+"を検討リストに追加しました")){
                location.reload();
            }
        }else{
            if(confirm(stock_no+"は登録済みです")){
                location.reload();
            }
        }
    }

    // Dateオブジェクトから日時を取得する共通処理(yyyy/MM/dd HH:mm:ss)
    dateToLocaleString = function(date){
        return [
        date.getFullYear(),
        date.getMonth() + 1,
        date.getDate()
        ].join( '/' ) + ' '
        + date.toLocaleTimeString();
    }
    
    set_history = function(control_no){
        var name = 'truskey_history';
        setCookie(name, control_no);
        var buf_array = $.cookie(name).split(",");
        buf_array.sort(
            function(a, b){
                var a_date = new Date(a.split(";")[1]);
                var b_date = new Date(b.split(";")[1]);
                if( a_date > b_date ){ return -1; }
                if( a_date < b_date ){ return 1; }
                return 0;
            }
        );
        // 20件以上ある場合、古いものから削除
        for (var i = 10 ; i < buf_array.length ; i++){
            deleteCookie(name, buf_array[i].split(";")[0]);
        }
    }

});