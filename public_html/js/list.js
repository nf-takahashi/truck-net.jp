$(document).ready(function () {
    $('.model_year, .rows_per_page, .sort_record, .layout').on('change', function () {
        if ($(this).attr('id') === 'layout') {
            var layout = $('option:selected', this).attr('data-value');
            saveLayout(layout);
        }
        var modelUrl = $(this).val();
        window.location.href = modelUrl;
    });

    $(".keyword").keypress(function (e) {
        if (e.which == 13) {
            var value = $(this).val();
            searchTransition(value);
        }
    });

    $('[dom=btn_seach]').on('click', function(){
        var value = $(this).prev('.filter').find('input:text.keyword').val();
        searchTransition(value);
    });

    // Check cookie for filter window.
    if( $.cookie('filter_window') == 'on' ){
      $('body').addClass('show_filter show_filter_default');
    }

});

function searchTransition(value){
    var url = $("input[name='url']").val();
    if (url != '' && url != undefined) {
        if (url.indexOf('?') > -1) {
            var str = url.split('?');
            console.log(str);
            var paramUrl = str[1].split('&');

            if (checkHasKeywordSearch(paramUrl)) {
                paramUrl = executeWithKeyword(paramUrl, value);
            } else {
                if (value) {
                    paramUrl.push('word=' + value);
                }
            }

            window.location.href = '/list' + '?' + paramUrl.join('&');
        } else {
            if (value) {
                window.location.href = '/list' + '?' + 'word=' + value;
            }
        }
    } else {
        if (value) {
            window.location.href = '/list' + '?' + 'word=' + value;
        } else {
            window.location.href = '/list';
        }
    }
    return false;
}

function checkHasKeywordSearch(paramUrl)
{
    var hasKeyword = false;
    $.each(paramUrl, function (i ,e) {
        var param = e.split('=');
        if (param[0] == 'word') {
            hasKeyword = true;
        }
    });

    return hasKeyword;
}

function executeWithKeyword(paramUrl, value)
{
    $.each(paramUrl, function (i ,e) {
        if (e && e !== 'undefined') {
            var param = e.split('=');
            if (param[0] == 'word') {
                if (value) {
                    paramUrl[i] = param[0] + '=' + value;
                } else {
                    paramUrl.splice(i, 1);
                }
            }
        }
    });

    return paramUrl;
}
