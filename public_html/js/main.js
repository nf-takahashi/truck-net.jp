//グローバルな変数
//ライトボックスに表示するコメントを保存する配列
var $comment1 = [];
var $comment2 = [];
var $img = [];
var $branch = [];
var $icon = [];
var $name = [];
var $rome = [];
var $career = [];
var $lightBoxComment = [];
var $age = [];
var cameraFlg;
var cameraSize = "";
var BREAK_POINT = 640;
//画面読み込み時の処理
jQuery(function() {
	changeCamearaFlg();
	changeCameraJsImage();

	//XML通信開始
	getXML();
	//スライドギャラリー
	servayStartGalleryCanGo();
	//スクロールイベント管理開始
	scrollEventControl();
	//ヘッダーボタンイベント
	jQuery('.headerButton').each(function () {
		jQuery(this).find('li').on('click', scrollContents);
	});
	//トップへボタンイベント
	jQuery("#toTopScroll").on('click', scrollContents);
	//アイコンイベント
	/*jQuery('.icons').find('img').on({
										'mouseover': iconsOnMouseAction,
										'mouseout' : iconsOffMouseAction
									});
	*/

	setBoxAreaHeight();

	//スマホ用ヘッダーボタンイベント
	//jQuery('#phoneBtn a').on('click', phoneHeaderAction);

	//ライトボックスのコメント欄高さ、KURIYAMAという会社の高さ調整
	jQuery(window).on("resize", setBoxAreaHeight);
	//ギャラリーのリサイズ
	jQuery(window).on("resize", changeCameraJsImage);

	jQuery("#headerCloseButton").on("click", phoneHeaderAction);

});


/**
 * xmlの読み込み開始
 */
function getXML () {
	jQuery.ajax({
		url:'./sv/worker.xml',
		type:'get',
		dataType:'xml',
		timeout:3500,
		success:parseXml
	});
}


/**
 * XMLデータを取得した際の処理
 * @param  xml: XMLデータ
 * @param  status: 通信ステータス
 */
function parseXml(xml, status) {
	//タイムアウト時は、再取得
	if (status != 'success') {
		getXML();
		return;
	}

	jQuery(xml).find('worker').each(dispXML);
	//setMasonry();
	jQuery('#workerSection').find('.workerCol').on("click", showLightBox);
}


/**
 * XMLデータをそれぞれ変数に収める処理
 */
function dispXML () {
	//社員紹介オブジェクト構成要素を配列へ
	$comment1.push(jQuery.trim(jQuery(this).find('comment1').text()));
	$comment2.push(jQuery.trim(jQuery(this).find('comment2').text()));
	$img.push(jQuery.trim(jQuery(this).find('image').text()));
	$branch.push(jQuery.trim(jQuery(this).find('branch').text()));
	$icon.push(jQuery.trim(jQuery(this).find('icon').text()));
	$name.push(jQuery.trim(jQuery(this).find('name').text()));
	$rome.push(jQuery.trim(jQuery(this).find('romanize').text()));
	$career.push(jQuery.trim(jQuery(this).find('career').text()));
	$age.push(jQuery.trim(jQuery(this).find('age').text()));

	//ライトボックスに表示する中身を配列へ
	$lightBoxComment.push(jQuery.trim(jQuery(this).find('answer1').text()));
	$lightBoxComment.push(jQuery.trim(jQuery(this).find('answer2').text()));
	$lightBoxComment.push(jQuery.trim(jQuery(this).find('answer3').text()));
	$lightBoxComment.push(jQuery.trim(jQuery(this).find('answer4').text()));
	$lightBoxComment.push(jQuery.trim(jQuery(this).find('answer5').text()));
	$lightBoxComment.push(jQuery.trim(jQuery(this).find('answer6').text()));

	var workerObj = createWorkerObj();

	jQuery('#workerSection').append(workerObj);
}


/**
 * 社員オブジェクトを生成する処理
 * @return obj: jQueryオブジェクト
 */
function createWorkerObj () {

	//社員紹介の箱を生成
	var $parentDiv = jQuery('<div class="workerCol"></div>');
	//コメント生成
	var com = '<div><h2>' + $comment1[$comment1.length - 1] + '<br>' + $comment2[$comment2.length - 1] + '</h2></div>';
	var balloon = '<div><img src="./img/common/words_balloon.png"></div>'
	//顔写真
	var im = '<div id="workerImgArea"><img src="' + $img[$img.length - 1] + '"></div>';
	
	//名前等の左側
	var left = jQuery('<div class="workerLeftArea"></div>');

	var br = '<div><p><span class="bg_YELLOW"><span class="slash">' + $branch[$branch.length - 1] +' </span>' + $career[$career.length - 1] + 
	'</span><span class="ageSpan">【' + $age[$age.length - 1] + '】' + '</span></p></div>';
	var ps = '<p class="name">' + $name[$name.length - 1] + '</p>';
	var rm = '<p class="initial">' + $rome[$rome.length - 1] + '</p>';

	var ma = jQuery('<div class="workerNameArea"></div>');

	left.append(br).append(ps).append(rm);
	ma.append(left).addClass($icon[$icon.length - 1]);

	$parentDiv.append(com).append(balloon).append(im).append(ma);
	var obj = $parentDiv;
	return obj;
}


/**
 * ライトボックスを表示する
 */
function showLightBox () {
	var $bg = jQuery('#lightBoxBackground');
	var $lb = jQuery('#lightBox');
	var windowHeight = jQuery(window).height();
	var windowWidth = jQuery(window).width();

	//クリックされた要素の順番を取得
	var index = jQuery('.workerCol').index(this);
	//背景の高さ設定
	$bg.css("height", + "100%");
	if (windowWidth > 640) {

		//ライトボックスの位置設定
		$lb.css({
					height: windowHeight * 0.85 + 'px',
					top: (windowHeight * 0.15) / 1.5 + 'px',
				});
	} else {

		//ライトボックスの位置設定
		$lb.css({
					height: "100%",
					top: jQuery(window).scrollTop() + 20 + 'px'
				});
	
	}

	

	//表示内容の設定
	setLightBoxContents(index);

	//ライトボックスの表示
	$bg.css({display: 'block'}).stop().animate({opacity: 1}, 500);
	$lb.css({display: 'block'}).stop().animate({opacity: 1}, 500);

	//背景、もしくは閉じるボタンクリックでライトボックス非表示を設定
	$bg.on("click", hideLightBox);
	jQuery('#rbCloseButton').on("click", hideLightBox);
	jQuery('#rbLeftButton, #rbRightButton').on("click", changeLightBoxContents);

	setBoxAreaHeight();
}

/**
 * ライトボックスの中身を設定する処理
 * @param index クリックされたボックスの番号
 */
function setLightBoxContents (order) {
	var num = order;
	//いったんアイコンの指定を初期化
	jQuery('#lightBoxInfo_right').removeClass();

	jQuery('#lightBoxImage img').attr("src", $img[num]);
	var carrer = '<span class="bg_YELLOW"><span class="slash">' + $branch[num] +' </span>' + $career[num] + '</span>【' + $age[num] + '】';
	jQuery('#lightBoxCarrer').empty();
	jQuery('#lightBoxCarrer').append(carrer);
	jQuery('#lightBoxInfo_right').addClass("workerRightArea").addClass($icon[num]);
	jQuery('#lightBoxName').children('span').first().text($name[num]);
	jQuery('#lightBoxName').children('span').last().text($rome[num]);
	jQuery('#boxLeft h3').text($comment1[num] + $comment2[num]);

	var ans = jQuery('.answer');
	ans.each(function (index) {
		if (num == 0) {
			jQuery(this).text("A：" + $lightBoxComment[index]);
		} else {
			jQuery(this).text("A：" + $lightBoxComment[index + (num * 6)]);
		}
	});

}

/**
 * ライトボックスの中身を変更する処理
 * @param event イベントオブジェクト
 */
function changeLightBoxContents (event) {
	//ボタンのIDを取得
	var id = event.target.id;
	//絶対にかぶらない物から順序を取得
	var name = jQuery('#lightBoxName').children('span').first().text();

	//名前から表示している番号を取得
	for (var counter = 0; counter < $name.length; counter++) {
		if (jQuery.trim($name[counter]) === name) {
			break;
		}
	}

	//左ボタンならインデックスをマイナス１。逆ならプラス１
	if (id == "rbLeftButton") {
		var index = (counter == 0) ? ($name.length - 1) : counter -= 1;
	} else  {
		var index = (counter == $name.length - 1) ? 0 : counter += 1;
	}

	jQuery('#lightBox').stop().animate({"opacity": "0"}, 400, "easeInSine", function () {
		setLightBoxContents(index);
		jQuery('#lightBox').stop().animate({"opacity": "1"}, 400, "easeInSine");
	});
}

/**
 * ライトボックスのコメント欄の高さを調整する処理
 */
function setBoxAreaHeight () {
	var $window = jQuery(window)
	var width = $window.width();
	var $lbi = jQuery('#lightBoxImage');
	//var lh = jQuery('#lightBoxImage').width() * 0.935;
	var lh = $lbi.height();

	if (lh == 0) {
		lh = $lbi.width() * 0.935;
	}

	var gh = jQuery('#growth_left').height() + 1;
	var vh = jQuery('#vision_right').height() + 1;
	var head = jQuery('#lightBoxInfo_left').height() + jQuery('#boxLeft h3').height();

	if (width > 640) {
		//噴出しテキストのマージン量
		head = head + (420 * 0.021);
		//噴出しのマージン量
		head = head + (420 * 0.03);
		//コメント欄上部のマージン量
		head = head + (420 * 0.045);
		lh = lh - head + "px";
	} else {
		//lh = $window.height() - lh * 2;
		lh = "100%";
	}

	//ライトボックスのコメント高さ
	jQuery('#answerArea').css("height", lh);
	//成長性の高さ
	jQuery('#growth_right').css("height", gh + "px");
	//ビジョンの高さ
	jQuery('#vision_left').css("height", vh + "px");
}

/**
 * ライトボックスを非表示にする
 */
function hideLightBox () {

	var $bg = jQuery('#lightBoxBackground');
	var $lb = jQuery('#lightBox');

	//ライトボックスの非表示
	$bg.stop().animate({opacity: 0}, 500, function () {
		$bg.css("display", "none");
	});
	$lb.stop().animate({opacity: 0}, 500, function () {
		$lb.css("display", "none");
	});

	jQuery('#rbLeftButton, #rbRightButton').off("click");
}

/**
 * スライドギャラリーを開始しても大丈夫かチェック
 */
function servayStartGalleryCanGo () {
	var ag = getUserAgent();
		if (ag == "pc") {
			startSlideGallery(jQuery('.cGallery'), 'left');
			startSlideGallery(jQuery('.bGallery'), 'left');
		}
}

/**
 * ユーザエージェントの取得
 * @return デバイスの種類
 */
function getUserAgent () {
	var agent = false;
	if((navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') == -1) || navigator.userAgent.indexOf('iPad') > 0){
		agent = 'tb';
	} else if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 ||
		 (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0)){
		agent = 'sp';
	} else {
		agent = 'pc';
	}

	return agent;
}

/**
 * スライドギャラリー開始
 */
function startSlideGallery (gallery, side) {
	var fps = 24;
	var target = gallery.children('li:first-child');
	var width
	var margin;
	var val;

	if (side === 'left') {
		width = (-1) * target.width();
		val = (-1);
		margin = 0;
	} else {
		width = target.width();
		val = 1;
		margin = -400;
	}

	animation();

	//marginLeftの位置を少しずつずらす
	function animation () {
		setTimeout(function () {
			target.css("marginLeft", margin);
			margin += val;

			if (side === 'left' && margin < width) {
				margin = 0;
				gallery.children('li:last-child').after(target);
				target.css('marginLeft', '0px');
				target = gallery.children('li:first-child');
			} else if (side === 'right' && margin == -2) {
				margin = -400;
				gallery.children('li:first-child').before(gallery.children('li:last-child'));
				target.css('marginLeft', '0px');
				target = gallery.children('li:first-child');
			}

			//再帰
			animation();

		}, fps);
	}

}


/**
 * スクロールイベントにより発生させる処理の管理
 */
function scrollEventControl () {
	jQuery(window).scroll(function() {

		if (jQuery(this).scrollTop() > 2200) {
			jQuery('#toTopScroll').css("display", "block");
			jQuery('#toTopScroll').stop().animate({"opacity":"1"}, 300, 'easeInOutQuad');
		} else {
			jQuery('#toTopScroll').css({
				"opacity": "0",
				"display": "none"
			});
		}

	});
}

/**
 * 社内制度アイコンマウスオーバー時の処理
 * @param  evt：イベントオブジェクト
 */
function iconsOnMouseAction (evt) {
	//iconsImageChangeFunction(jQuery(this), evt);
}


/**
 * 社内制度アイコンマウスアウト時の処理
 * @param  evt：イベントオブジェクト
 */
function iconsOffMouseAction (evt) {
	//iconsImageChangeFunction(jQuery(this), evt);
}


/**
 * 社内制度アイコンマウス関連イベント動作処理
 * @param  obj：jQueryオブジェクト
 * @param  evt：イベントオブジェクト
 */
function iconsImageChangeFunction (obj, evt) {
	var img = obj.attr('src');
	var tooltip = obj.parent('figure').next();

	if (evt.type === 'mouseover') {
		img = img.replace('.gif', '_on.gif');
		tooltip.animate({"opacity": 1}, 300);
	} else if (evt.type === 'mouseout') {
		img = img.replace('_on', '');
		tooltip.animate({"opacity": 0}, 300);
	}

	obj.attr('src', img);
}


/**
 * ヘッダーボタン押下でのスクロール処理
 */
function scrollContents () {

	var $id;

	if (jQuery(this).get(0).id == "" || jQuery(this).get(0).id == undefined ) {
		$id = jQuery(this).find('a').get(0).id;
		console.log("a")
	} else {
		$id = jQuery(this).get(0).id ;
	}

	var pos;
	var id;

	switch ($id) {
		case "toWorkers":
			id = "#workers";
			break;
		case "toWorkersSp":
			id = "#workers";
			break;
		case "toAspiration":
			id = "#aspiration";
			break;
		case "toAspirationSp":
			id = "#aspiration";
			break;
		case "toCompanySystem":
			id = "#aboutCompany";
			break;
		case "toCompanySystemSp":
			id = "#aboutCompany";
			break;
		case "toRecInformation":
			id = "#recInformation";
			break;
		case "toRecInformationSp":
			id = "#recInformation";
			break;
		default:
			id = $id;
	}

	if (id === 'toWorkers') {
		pos = jQuery('#slide').height() + jQuery('header').height() - 20;
	} else if (id === "toTopScroll") {
		pos = 0;
	} else {
		pos = jQuery(id).offset().top - jQuery('header').height();
	}

	jQuery('html,body').animate({scrollTop:pos}, 700, 'easeInOutQuad');
}

/**
 * スマホ画面用ヘッダからのボタン呼び出し
 */
function phoneHeaderAction () {
	$navArea = jQuery("#header_navArea_sp");

	if ($navArea.css("opacity") == 0) {
		jQuery(this).css("backgroundImage", 'url("./img/sp/header_closeBtn.png")');
		$navArea.css({
			"opacity" : "1",
			"display": "block"
		});
		jQuery("#header_right").css({
			"opacity": "1",
			"display": "block"
		});
	} else {
		jQuery(this).css("backgroundImage", 'url("./img/sp/header_openBtn.png")');
		$navArea.css({
			"opacity" : "0",
			"display": "none"
		});
		jQuery("#header_right").css({
			"opacity": "0",
			"display": "none"
		});
	}
	
}

/**
 * ウィンドウサイズを取得し、カメラフラグを変更する処理
 */
function changeCamearaFlg () {
	var w = $(window).width();
	if (w <= BREAK_POINT) {
		cameraFlg = true;
	} else {
		cameraFlg = false;
	}
}

/**
 * 640px以下の場合のメインギャラリー画像切り替え処理
 */
function changeCameraJsImage () {
	var w = $(window).width();
	var $div1;
	var $div2;
	var $div3;

	if (w <= BREAK_POINT && cameraFlg) {
		jQuery("#slide").empty();
		$div1 = jQuery('<div></div>');
		$div2 = jQuery('<div></div>');
		$div3 = jQuery('<div></div>');

		$div1.attr("data-src", "img/sp/slide01_sp.jpg");
		$div2.attr("data-src", "img/sp/slide02_sp.jpg");
		$div3.attr("data-src", "img/sp/slide03_sp.jpg");

		jQuery("#slide").append($div1).append($div2).append($div3);
		cameraFlg = false;
		cameraSize = "141%";
		//カメラプラグインの設定
		setcameraPlugin();
	} else if (w > BREAK_POINT && !cameraFlg) {
		jQuery("#slide").empty();
		$div1 = jQuery('<div></div>');
		$div2 = jQuery('<div></div>');
		$div3 = jQuery('<div></div>');

		$div1.attr("data-src", "img/pc/slide_1.jpg");
		$div2.attr("data-src", "img/pc/slide_2.jpg");
		$div3.attr("data-src", "img/pc/slide_3.jpg");
		
		jQuery("#slide").append($div1).append($div2).append($div3);
		cameraFlg = true;
		cameraSize = "50%";
		//カメラプラグインの設定
		setcameraPlugin();
	}
}

/**
 * Topギャラリーのプラグイン設定
 */
function setcameraPlugin () {
	var t = jQuery('#slide');
		t.camera({
			alignment:'topCenter',
			height: cameraSize,
			loader: 'none',
	        	thumbnails: false,
	        	hover:true,
	        	fx: 'simpleFade',
	        	time: 4000,
	        	playPause: false,
	        	imagePath: 'img/',
	        	loaderBgColor: '#8EB1CC',
	        	loaderColor:'#555555',
	        	hover: false,
	                onEndTransition: function(){
	                        var ind = t.find('.camera_target .cameraSlide.cameracurrent').index();
	                }
		});

	jQuery('#slide').css("marginBottom", "0");
}