<?php
require_once $_SERVER['DOCUMENT_ROOT']. "/settings/settings.php";
$pageTitle = "よくあるご質問";

$content=<<<HERE
<div class="breadclumb">
<ul itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
    <li><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
        <li><a href="/trucks/index.php">中古トラック</a></li>
    <li><span itemprop='title'>$pageTitle</li>
</ul>
</div>
<article class="faq">
        <div class="wrap">
            <h1 class="title">よくあるご質問</h1>


            <div class="index_q">
                <dl>
                    <dt>ご購入に関して</dt>
                    <dd>
                        <ul style="font-size: 14px; text-decoration:underline; text-align:left;">
                            <li style="padding: 0 0 10px 50px;"><a href="#faq01">中古トラックの購入ははじめてだけど、品質は大丈夫？</a></li>
                            <li style="padding: 0 0 10px 50px;"><a href="#faq02">総額はいくらですか？</a></li>
                            <li style="padding: 0 0 10px 50px;"><a href="#faq03">取り置き(押さえる事)は可能でしょうか？</a></li>
                            <li style="padding: 0 0 10px 50px;"><a href="#faq04">遠方のため現車確認が出来ないのですが。</a></li>
                            <li style="padding: 0 0 10px 50px;"><a href="#faq05">ローン・リースは利用できますか？</a></li>
                            <li style="padding: 0 0 10px 50px;"><a href="#faq06">車検や予備検査取得、塗装・架装・カスタマイズはできますか？</a></li>
                            <li style="padding: 0 0 10px 50px;"><a href="#faq07">納車までどれくらいの時間がかかりますか？</a></li>
                            <li style="padding: 0 0 10px 50px;"><a href="#faq08">必要な書類手続きを教えてください。</a></li>
                            <li style="padding: 0 0 10px 50px;"><a href="#faq09">発注後のキャンセルや返品は可能ですか？</a></li>
                        </ul>
                    </dd>
                    <dt>サービスに関して</dt>
                    <dd>
                        <ul style="font-size: 14px; text-decoration:underline; text-align:left;">
                            <li style="padding: 0 0 10px 50px;"><a href="#faq04">ホームページに掲載されている車両以外に車両はありますか？</a></li>
                            <li style="padding: 0 0 10px 50px;"><a href="#faq10">下取りや買取りだけでもお願いできますか？</a></li>
                        </ul>
                    </dd>
                </dl>
            </div>
            <h2 class="title_sec">ご購入に関して</h2>
            <div id="faq01">
                <div class="question_header">
                    中古トラックの購入ははじめてだけど、品質は大丈夫？
                </div>
                <div class="answer_header">
                    トラック仕入経験15年のベテランスタッフが、お客様目線で厳選したトラックのみを販売させていただいております。<br>
                    整備士経験20年の専門スタッフが、目視での点検を行い、社内基準に合ったトラックのみを販売させていただいております。<br>
                    中古車販売20年のベテランスタッフが、豊富な知識と経験を基にわかりやすくご説明いたします。
                </div>
            </div>
            <div id="faq02">
                <div class="question_header">
                    総額はいくらですか？
                </div>
                <div class="answer_header">
                    表示価格が総額となります。ただし、車検切れの場合は、お客様ご自身で車検を通して頂く事となります。<br>
                    出来るだけお安くご提供するため、ご購入後、お客様が必要な箇所のみ整備工場様などでお手入れして頂き、稼働させていただくことを前提として販売しております。
                </div>
            </div>
            <div id="faq03">
                <div class="question_header">
                    取り置き(押さえる事)は可能でしょうか？
                </div>
                <div class="answer_header">
                    小人数で運営しているため、商談管理をしておりません。お取り置き等優先権もございません。<br>
                    平たく言うと、<span style="font-weight:bold">「早い者勝ち」</span>となります。
                </div>
            </div>
            <div id="faq04">
                <div class="question_header">
                    遠方のため現車確認が出来ないのですが。
                </div>
                <div class="answer_header">
                    実車確認にご来店できないお客様には、LINEやメールにて追加撮影等の詳細な写真をお送りさせていただきます。<br>
                    車両状態については出来る限り詳しくご説明いたしますが、現車優先である事はご承知おき下さい。
                </div>
            </div>
            <div id="faq05">
                <div class="question_header">
                ローン・リースは利用できますか？
                </div>
                <div class="answer_header">
                    現金払い、または銀行振り込みにてお願いします。<br>
                    ※審査や書類やり取りを行わない事でお安くご提供する仕組みとしておりますため、ローンは取り扱っておりません。ご了承ください。
                </div>
            </div>
            <div id="faq06">
                <div class="question_header">
                    車検や予備検査取得、塗装・架装・カスタマイズはできますか？
                </div>
                <div class="answer_header">
                    追加作業をお受けしない事でお安くご提供する仕組みとしておりますため、すべてお断りさせて頂いております。ご了承下さいませ。
                </div>
            </div>
            <div id="faq07">
                <div class="question_header">
                    納車までどれくらいの時間がかかりますか？
                </div>
                <div class="answer_header">
                    ご来店時、現金払いの場合は、回送ナンバーをお持ちいただければその場でのってお帰り頂く事も出来ます。<br>
                    銀行振込の場合には、ご入金確認後にお引渡し可能となります。
                </div>
            </div>
            <div id="faq08">
                <div class="question_header">
                    必要な書類手続きを教えてください。
                </div>
                <div class="answer_header">
                    トラックご購入にあたってご準備頂く書類は以下の書類となります。<br>
                    印鑑証明書 、譲渡証明書、委任状、車庫証明、増車申請（お車やお客さまにより異なります）<br>
                    <br>
                    その他ご心配な点がございましたらお気軽にお問合せくださいませ。
                </div>
            </div>
            <div id="faq09">
                <div class="question_header">
                    発注後のキャンセルや返品は可能ですか？
                </div>
                <div class="answer_header">
                    申し訳ございません。契約後のキャンセル、返品は承っておりません。
                </div>
            </div>
            <h2 class="title_sec">サービスに関して</h2>
            <div id="faq04">
                <div class="question_header">
                    ホームページに掲載されている車両以外に車両はありますか？
                </div>
                <div class="answer_header">
                    掲載準備中や搬送中等の理由によりホームページに掲載していない車両もございます。<br>
                    ご希望の車両がございましたら、お気軽にお問合せください。<br>
                    専門スタッフが直接在庫車両を確認し、対応させて頂きます。<br>
                    ご希望に沿う車両がない場合は、条件をお伺いし順次ご案内いたします。<br>
                </div>
            </div>
            <div id="faq10">
                <div class="question_header">
                    下取りや買取りだけでもお願いできますか？
                </div>
                <div class="answer_header">
                    もちろん下取りや買取りも可能です。査定資格を持った専門スタッフが無料で出張査定いたします！<br>
                    ローン中でもOK。また、カスタム車も大歓迎。即金での買取りも承っております。<br>
                    まずはお気軽にご相談くださいませ。<br>
                </div>
            </div>


        </div>
</article>
HERE;
HtmlSource::Output($content,$pageTitle); // HTML出力
?>