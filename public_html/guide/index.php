<?php
require_once $_SERVER['DOCUMENT_ROOT']. "/settings/settings.php";
$pageTitle = "ご購入の流れ";

$content=<<<HERE
<div class="breadclumb">
<ul itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
    <li><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
        <li><a href="/trucks/index.php">中古トラック</a></li>
    <li><span itemprop='title'>$pageTitle</li>
</ul>
</div>
<article class="guide">
      <div class="wrap">
          <h1 class="title">ご購入の流れ</h1>
          <div class="messege_box">ご購入の流れは下記のとおりです。<br>ご不明な点などありましたらお気軽にお問い合わせください。中古車販売歴20年の小佐田（コサダ）が親切ていねいにご対応致します。
          </div>

          <dl>
              <dt><span>STEP<strong>1</strong></span>問い合わせる</dt>
              <dd>
                  お問い合わせ時に、中古トラックに書かれている5桁の「ストックナンバー」をお知らせください。<br>
                  <br>
                <span style="font-weight:bold;">お電話の場合</span><br>
                <a href="tel:08041706759">080-4170-6759</a> へお電話下さい。<br>中古車販売歴20年の小佐田（コサダ）が親切ていねいにご対応致します。<br><br>
                <span style="font-weight:bold;">LINEの場合</span><br>
                080-4170-6759をともだち登録してから、メッセージをお送りください。<br>中古車販売歴20年の小佐田（コサダ）が親切ていねいにご対応致します。<br><br>
                <span style="font-weight:bold;">メールの場合</span><br>
                <a href="mailto:kosada@kuriyama-truck.com">kosada@kuriyama-truck.com</a> までお願いします。<br>中古車販売歴20年の小佐田（コサダ）が親切ていねいにご対応致します。<br><br>
                <br class="clear">
              </dd>
          </dl>


          <dt><span>STEP<strong>2</strong></span>実車確認する</dt>
          <dd>
            上記お問合せ方法にて、実車確認希望をご依頼ください。<br>
            　※　現状販売方式にて現車状態優先とさせて頂いておりますので、ぜひ一度お客様の目でトラックをご確認下さい。
            <br class="clear">
          </dd>
          <dt><span>STEP<strong>3</strong></span>契約する</dt>
          <dd>
            ご納得いただけましたら、ご契約となります。<br>
            　※　Yahoo!オークションにも出品しております。そちらに入札者様がいらっしゃる場合は、オークションへのご参加、または即決価格でご契約いただくようになります。
             <br class="clear">
          </dd>
          <dt><span>STEP<strong>4</strong></span>支払いをする</dt>
          <dd>
            現金払い、または銀行振り込みをお願いします。<br>
            　※　ローンは取り扱っておりません。あらかじめご了承ください。<br>
            <br>
            <span style="font-weight:bold;">お振込先</span><br>
            三菱ＵＦＪ銀行　小松川支店　普通口座　０６００６８５<br>
            みずほ銀行　西葛西支店　普通口座　１３３２１１８<br>
            クリヤマジドウシャコウギョウ（カ<br>
            <br class="clear">
          </dd>
          <dt><span>STEP<strong>5</strong></span>陸送手配する</dt>
          <dd>
            全額お支払い完了後にお客様へ納車となります。<br>	
            　※　車両の書類関係は、現金でのお支払い時または銀行振込完了後の発送となります。<br>
            <br>
            <span style="font-weight:bold;">お客様ご自身でのお引取り</span><br>
            当社までお越し頂きます。<br>
            <br>
            <span style="font-weight:bold;">当社での陸送手配代行の場合</span><br>
            当社で回送専門業者への依頼代行もご相談下さい。
          <br class="clear">
          </dd>
      </div>
  </article>
HERE;
HtmlSource::Output($content,$pageTitle); // HTML出力
?>