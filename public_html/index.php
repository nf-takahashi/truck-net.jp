<?php
require_once $_SERVER['DOCUMENT_ROOT']. "/settings/settings.php";

//新入庫一覧
$url = $listUrl . $defaultQuery . $defaultSort. "&limit=8";
$json = file_get_contents($url);
$array = json_decode($json,true); //結果JSONを配列をにエンコードし出力
$topArrival = "";
$i = 1;

// ■■ここから■■■■■■■■■■■■■
foreach($array["datas"] as $value){
    require($_SERVER['DOCUMENT_ROOT']."/settings/top_column.php");
    // spec

$topArrival .= <<<HERE
            <li class="topItem">
                <a href='/trucks/detail.php?id=$id' target='_blank'>
                  <dl>
                    <dt class="topItem_title">
                      $selling_title
                    </dt>
                    <dd>
                      <div class="topItem_column">
                        <div class="topItem_image">
                            <img src="https://images.kuriyama-truck.com/images/vehicle/$id/thumbnail_0/1_1.jpg" alt="" width="220" height="170" class="jsReplaceNoImage" onerror="this.src='/images/noimage_top.png'">
                        </div>
                        <div class="spec_area">
                          <dl class="spec stock_number">
                              <dt>ストック番号</dt><dd>$shape-$id</dd>
                          </dl>
                          <dl class="spec">
                              <dt>年式</dt><dd>$model_year $model_year_month</dd>
                          </dl>
                          <dl class="spec">
                              <dt>型式</dt><dd>$model</dd>
                          </dl>
                          <dl class="spec">
                              <dt>走行</dt><dd>$mileage</dd>
                          </dl>
                          <dl class="spec spec_price">
                              <dt>お引渡し総額(消費税込み)</dt><dd class="topItem_price">$price</dd>
                          </dl>
                        </div>
                      </div>
                    </dd>
                  </dl>
                </a>
            </li>

HERE;
}
$content=<<<HERE
        <!-- start vacation notice-->
        <div style="max-width: 1140px;font-size: 121%; border: 3px solid #900; margin: 30px auto 20px auto; padding:0; text-align: left; line-height:150%;">
        	<h4 style="background: #900; color: #FFF; padding: 2px 0 2px 5px;">冬季休業日程のおしらせ</h4>
        	<p style="background: #FFE; padding: 2px 0 2px 5px; color: #000; margin:0;">
        		12/29(土)～1/6(日)までの期間、冬季休業とさせていただきます。
                休業期間中のお問合せは <a href="/contact/" style="text-decoration:underline;">メール</a> よりどうぞ。
        	</p>
        </div>
        <!-- end vacation notice-->


        <h2 class="title">特選車輌ピックアップ！</h2>
        <div class="top_list">
            <ul>
                $topArrival
            </ul>
        </div>


        $search_icon
        <div class="banners">
          <img src="/images/banner/detail_concept.png">
        </div>

HERE;
HtmlSource::Output($content,"HOME"); // HTML出力
?>
