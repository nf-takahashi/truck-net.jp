<?php
require_once $_SERVER['DOCUMENT_ROOT']. "/settings/settings.php";

switch($cat){
    case "1,2":
        $catName = "ウイング・バン";
        break;
    case "4,5,6":
        $catName = "平・クレーン・ダンプ";
        break;
    case "8,9,10,11":
        $catName = "その他";
        break;
    case 1:
        $catName = "ウイング";
        break;
    case 2:
        $catName = "バン";
        break;
    case 3:
        $catName = "冷凍ウイング・バン";
        break;
    case 4:
        $catName = "クレーン・セルフ";
        break;
    case 5:
        $catName = "平ボディー";
        break;
    case 6:
        $catName = "ダンプ";
        break;
    case 7:
        $catName = "トラクタ・トレーラー";
        break;
    case 8:
        $catName = "パッカー・清掃関係";
        break;
    case 9:
        $catName = "アームロール";
        break;
    case 10:
        $catName = "ミキサー車";
        break;
    case 11:
        $catName = "その他";
        break;
    default:
        $catName = "すべて";
}
$pageTitle = $catName;
//一覧
$url = $listUrl . $defaultQuery. $defaultSort. $defaultLimit. "&shape=".$cat;
$json = file_get_contents($url);
$array = json_decode($json,true); //結果JSONを配列をにエンコードし出力
$count = $array["count"];
$listHtml = "";

foreach($array["datas"] as $value){
    require $_SERVER['DOCUMENT_ROOT']. "/settings/list_column.php";
$listHtml .= <<<HERE
		<li class="topItem">
  		<a href='/trucks/detail.php?id=$id' target='_blank'>
        <dl>
          <dt class="topItem_title">
            $selling_title
          </dt>
          <dd>
            <div class="topItem_column">
              <div class="topItem_image">
                  <img src="https://images.kuriyama-truck.com/images/vehicle/$id/thumbnail_0/1_1.jpg" alt="" width="220" height="170" class="jsReplaceNoImage" onerror="this.src='/images/noimage_top.png'">
              </div>
              <div class="spec_area">
                <dl class="spec stock_number">
                    <dt>ストック番号</dt><dd>$shape-$id</dd>
                </dl>
                <dl class="spec">
                    <dt>年式</dt><dd>$model_year $model_year_month</dd>
                </dl>
                <dl class="spec">
                    <dt>型式</dt><dd>$model</dd>
                </dl>
                <dl class="spec">
                    <dt>走行</dt><dd>$mileage</dd>
                </dl>
                <dl class="spec spec_price">
                    <dt>お引渡し総額(消費税込み)</dt><dd class="topItem_price">$price</dd>
                </dl>
              </div>
            </div>
          </dd>
        </dl>
  		</a>
		</li>
HERE;
			}
$content=<<<HERE
			<div class="breadclumb">
				<ul itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
					<li><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
						<li><a href="/trucks/index.php">中古トラック</a></li>
					<li><span itemprop='title'>$catName</li>
				</ul>
			</div>
			$search_icon
			<article class="product_list">
				<div class="title_list_area">
					<h1 class="title">$catName</h1>
				</div>
				<div class="head_group">
					<div class="result">
						<div class="number"><span>$count</span>台</div>
					</div>
				</div>
        <div class="top_list">
  				<ul>
  					$listHtml
  				</ul>
        </div>
			</article>

			<script src="/js/list.js?ver=180606"></script>
			<script src="/js/cookie.js"></script>
			<script src="/js/jquery.cookie.js"></script>
HERE;

HtmlSource::Output($content,$pageTitle); // HTML出力

?>
