<?php

require_once $_SERVER['DOCUMENT_ROOT']. "/settings/settings.php";
$imgHtml = "";
$zoomHtml = "";

//サムネイル一覧
$url = $imageUrl."&id=". $_GET["id"];
$json = file_get_contents($url);
$arr = json_decode($json,true);
for($i=0; $i <= count($arr["datas"]["car_infos"][0]["car_info"]["images"]["thumbnail_1"])-1 ; $i++){
    $pos = strrpos($arr["datas"]["car_infos"][0]["car_info"]["images"]["thumbnail_1"][$i]["img_path"],"/9_"); //不具合画像(9_)は非表示
    if($pos === FALSE){
        $imgHtml .= ("<li><img src=\"".$arr["datas"]["car_infos"][0]["car_info"]["images"]["thumbnail_0"][$i]["img_path"]."\" ></li>\r\n");
        $zoomHtml .= ("<li><img data-lazy=\"".$arr["datas"]["car_infos"][0]["car_info"]["images"]["zoom"][$i]["img_path"]."\" ></li>\r\n");
      }
}
//JSONからスペックデータの取得
$url = $detailUrl."&id=". $_GET["id"];
$json = file_get_contents($url);
$decode = json_decode($json,true); //結果JSONを配列をにエンコードし出力
$value = $decode["data"];
//print("<pre>");
//var_dump($array);
//print("</pre>");

require($_SERVER['DOCUMENT_ROOT']. '/settings/detail_column.php');
$pageTitle = $manufacturer." ".$model." ".$shape." ".$model_year.$model_year_month." ".$mileage."[".$id."]";
/*
foreach ($body_info as $value) {
    $bodyInfoHtml .= "<tr><th style='min-width:110px'>".$value["key"]."</th><td style='min-width:180px'>";
    foreach($value as $head => $atai){
        $bodyInfoHtml .= $head != "key"? "<span>".$atai["key"]."</span> ".$atai["value"]."<br>": "";
    }
    $bodyInfoHtml .= "</td></tr>";
}
*/
$content=<<<HERE
        <!-- start main -->
        <div class="breadclumb">
        <ul itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <li><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
                <li><a href="/trucks/index.php">中古トラック</a></li>
            <li><span itemprop='title'>$pageTitle</li>
        </ul>
    </div>

		<div class="search_icon">
			<h2 class="title_search"><span>上物形状</span>で探す</h2>
			<ul class="search_icon_list">
				<li>
					<a href='/trucks/?cat=1,2'>
						<div class="icon"><img src="/images/icon/icon01.png" alt=""></div>
						<div class="search_icon_text">ウイング<br>バン</div>
					</a>
				</li>
				<li>
					<a href='/trucks/?cat=3'>
						<div class="icon"><img src="/images/icon/icon23.png" alt=""></div>
						<div class="search_icon_text">冷蔵冷凍車</div>
					</a>
				</li>
				<li>
					<a href='/trucks/?cat=4,5,6'>
						<div class="icon"><img src="/images/icon/icon04.png" alt=""></div>
						<div class="search_icon_text">平・クレーン<br>ダンプ</div>
					</a>
				</li>
				<li>
					<a href='/trucks/?cat=7'>
						<div class="icon"><img src="/images/icon/icon14.png" alt=""></div>
						<div class="search_icon_text">トラクター<br>トレーラー</div>
					</a>
				</li>
				<li>
					<a href='/trucks/?cat=8,9,10,11'>
						<div class="icon"><img src="/images/icon/icon17.png" alt=""></div>
						<div class="search_icon_text">その他</div>
					</a>
				</li>
				<li>
					<a href='/trucks/?cat='>
						<div class="icon"><img src="/images/icon/icon18.png" alt=""></div>
						<div class="search_icon_text">すべて</div>
					</a>
				</li>
			</ul>
		</div>

        <h1 class="detail_title">$selling_title</h1>
        <div class="detil_top_column">
          <div class="detil_top_image">
            <img class="thumbnail_0" src="https://images.kuriyama-truck.com/images/vehicle/$id/zoom/1_1.jpg" onerror="this.src='/images/noimage_top.png'">
          </div>
          <p class="detil_top_text">$remarks</p>
        </div>
        <dl class="detil_price">
          <dt><span class="detil_price_title">ハダカ売り専門店</span><span class="notice">車検・整備・塗装等はお受けしておりません。安く販売するためなのでどうぞご理解下さい。</span></dt>
          <dd>$price<span class="notice">お引渡し総額 (消費税込み)</span></dd>

        </dl>
        <div class="detail_spec_area spec_area">
        <dl class="spec">
            <dt>ストック番号</dt><dd>$id</dd>
          </dl>
          <dl class="spec">
            <dt>年式</dt><dd>$model_year $model_year_month</dd>
          </dl>
          <dl class="spec">
            <dt>メーカー</dt><dd>$manufacturer</dd>
          </dl>
          <dl class="spec">
            <dt>型式</dt><dd>$model</dd>
          </dl>
          <dl class="spec">
            <dt>ミッション</dt><dd>$shifts</dd>
          </dl>
          <dl class="spec">
            <dt>走行距離</dt><dd>$mileage</dd>
          </dl>
          <dl class="spec">
            <dt>車検</dt><dd>$inspect</dd>
          </dl>
          <dl class="spec">
            <dt>最大積載</dt><dd>$max_load_cap</dd>
          </dl>
          <dl class="spec">
            <dt>リサイクル料</dt><dd>$recycle_ticket</dd>
          </dl>
          <dl class="spec">
            <dt>内寸長</dt><dd>$length</dd>
          </dl>
          <dl class="spec">
            <dt>内寸幅</dt><dd>$width</dd>
          </dl>
          <dl class="spec">
            <dt>内寸高</dt><dd>$height</dd>
          </dl>
        </div>

  	    $contact
        <ul class="detail_images">
          $imgHtml
        </ul>
        <div class="image_big_overlay_wrap" dom="image_big_overlay">
            <div class="image_big_overlay">
                <ul dom="image_big_overlay_slick">
                  $zoomHtml
                </ul>
                <div class="close_modal" dom="close_modal"></div>
            </div>
        </div>

  	    $contact

        <div class="point">
          <dl>
            <dt>点検</dt>
            <dd>機関系の主要部位についてのみ点検しております。<br>コストの関係で点検範囲は絞っておりますが、弊社スタッフが厳選したトラックです。</dd>
          </dl>
          <dl>
            <dt>程度</dt>
            <dd>記載以外の点検は行っておりませんので、ご自身の目で見ていただく事をおすすめします。</dd>
          </dl>
          <dl>
            <dt>修復歴</dt>
            <dd>修復歴がある場合も、お客様にお出し出来る基準以上のお車のみ出品しております。</dd>
          </dl>
          <dl>
            <dt>ながれ</dt>
            <dd>実車確認⇒　入金⇒　引取</dd>
          </dl>
          <dl>
            <dt>総額</dt>
            <dd>
              表示価格のみ<br>車検・整備・塗装等はお受けしておりません。安く販売するためなのでどうぞご理解下さい。<br>※ 消費税およびリサイクル料は含まれておりますのでご安心ください。<br>※ 陸送手配が必要な場合はお客様でお願いします。下記に料金目安表ございます。
              <!--
              <div class="transport_table">
                <img src="/images/banner/detail_postage.png">
              </div>
              -->
            </dd>
          </dl>
        </div>

HERE;
HtmlSource::Output($content,$pageTitle); // HTML出力
?>
