<?php
require_once $_SERVER['DOCUMENT_ROOT']. "/settings/settings.php";
$pageTitle = "お問い合わせ";

$content=<<<HERE
<div class="breadclumb">
<ul itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
    <li><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
        <li><a href="/trucks/index.php">中古トラック</a></li>
    <li><span itemprop='title'>$pageTitle</li>
</ul>
</div>
<article class="contact" style="padding-bottom:100px;border-bottom:2px solid #555;">
    <div class="wrap">
        <h1 class="title">お問い合わせ</h1>
        <p style="font-size: 140%;">
            お問合せは下記宛てにお願いいたします。<br>
            中古車販売歴20年の小佐田（コサダ）が親切ていねいにご対応致します。<br><br>
        </p>
        $contact
        <!--
        <h1 class="title">お問い合せフォーム</h1>
            <p>お客様の情報とお問合せ内容を入力し、送信ボタンを押してください。</p>
            <form id="form_contact" class="form" action="/contact/3" method="POST">
                <input type="hidden" name="uid" id="uid" value="">
                <input type="hidden" name="contact_type_car" value="">
                <dl>
                    <dt>
                        <label>お問合せの種類
                            <div class="reqired"><span>必須</span></div>
                        </label>
                    </dt>
                    <dd>
                                                    <label>
                                <input type="radio" name="contact_type" value="1" checked=""><span>価格や詳細を知りたい</span>
                            </label>
                                                    <label>
                                <input type="radio" name="contact_type" value="2"><span>トラックを売りたい</span>
                            </label>
                                                    <label>
                                <input type="radio" name="contact_type" value="4"><span>在庫のお問合せ</span>
                            </label>
                                                    <label>
                                <input type="radio" name="contact_type" value="5"><span>画像追加リクエスト</span>
                            </label>
                                                    <label>
                                <input type="radio" name="contact_type" value="6"><span>中古部品について</span>
                            </label>
                                                    <label>
                                <input type="radio" name="contact_type" value="7"><span>その他のご質問</span>
                            </label>
                                            </dd>
                </dl>
                <dl>
                    <dt>
                        <label>お名前
                            <div class="reqired"><span>必須</span></div>
                        </label>
                    </dt>
                    <dd>
                        <input name="name" type="text" id="name" value="" maxlength="255">
                        <span class="add">様</span>
                        <span class="add_info">※会社名からご入力ください</span>
                                            </dd>
                </dl>
                <dl>
                    <dt>
                        <label>ご希望の連絡方法
                            <div class="reqired"><span>必須</span></div>
                        </label>
                    </dt>
                    <dd>
                        <div class="column"><span>メールアドレス：</span>
                            <input name="mail" type="text" id="mail" value="" maxlength="255">
                                                        <span>確認用：</span>
                            <input name="mail2" type="text" id="mail2" value="" maxlength="255">
                                                    </div>
                        <div class="column"><span>お電話番号：</span>
                            <input name="tel" type="text" id="tel" value="" maxlength="20"><span>（例）01-2345-6789</span>
                                                                                </div>
                    </dd>
                </dl>
                <dl class="last">
                    <dt>
                        <label>お問合せ内容
                            <div class="reqired"><span>必須</span></div>
                        </label>
                    </dt>
                    <dd>
                        <dl class="form_example">
                            <dt class="form_inner_title">ご記入例</dt>
                            <dd>日野の４ｔウイング車を探しています。格納ゲート付きで予算は〇〇~〇〇万円くらい。使用時期は〇月からです。</dd>
                        </dl>
                        <textarea name="text" id="text" cols="65" rows="15" maxlength="1000"></textarea>
                                            </dd>
                </dl>
                <div class="send">
                    <button id="send_btn" type="submit" class="send_btn">入力内容を送信</button>
                    <p class="contact_sending">送信しています...</p>
                </div>
            </form>
-->
        </div>
</article>
HERE;
HtmlSource::Output($content,$pageTitle); // HTML出力
?>
