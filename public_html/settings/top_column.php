<?php

//変数の宣言
$id = $value['id'];
$shape = $value['shape'];
$selling_title = $value['selling_title'];
$mileage = $value['mileage']>0? sprintf('%.1f',($value['mileage'] / 10))."万km": "-";
$model_year = $value['model_year']? $value['model_year']. '年' : '';
$model_year_month = $value['model_year_month']? $value['model_year_month']. '月' : '';
$model = $value['model']? $value['model']. '' : '';
// price
if ($value["nego_status"] == '3') {
    $price = '<span class="souldout">ご成約</span>';
} elseif (is_numeric($value["app_price_any"])) {
    $price = '<span class="topItem_price_number">'.$value["app_price_any"] . '</span>万円[税込]';
} else {
    $price = '<span class="ask">ASK</span>';
}

?>