<?php
require_once $_SERVER['DOCUMENT_ROOT']. "/settings/search_icon.php";
require_once $_SERVER['DOCUMENT_ROOT']. "/settings/contact.php";

//変数の宣言
$listUrl = "https://images.kuriyama-truck.com/api/product/getProductList.php?app_key=57d7311526c8dd3e3a699bbeca55c145";
$detailUrl = "https://images.kuriyama-truck.com/api/product/getProductDetail.php?app_key=57d7311526c8dd3e3a699bbeca55c145";
$imageUrl = "https://images.kuriyama-truck.com/api/images/?id=&app_key=57d7311526c8dd3e3a699bbeca55c145";
$defaultQuery = "&ec_site=2&nego_status=1,8"; //必須条件 ワンプラ
$defaultSort = "&site_release_possible_date=1"; //デフォルト並び順 HP公開日新しい順
$defaultLimit = "&limit=999"; //デフォルト取得件数
$cat = isset($_GET["cat"])? $_GET["cat"]:""; //GET形状
$id = isset($_GET["id"])? $_GET["id"]:""; //GET ID

// クラスの定義
class HtmlSource // 共通HTMLの設定
{
	// ヘッダーの設定
	public function Output($content,$pageTitle)
	{
		switch ($_SERVER['REQUEST_URI']){
			case '/':
				$description = '<meta name="description" content="世界一安く、どこよりも稼げるトラックをお届けする事を使命とした東京都の中古トラック販売店です。ウイング車・バン車・冷凍車などの箱車、平ボディ・ダンプ・クレーンなどの建築系トラックを取り揃えております。私たちは栗山自動車グループの一員です。">';
				break;
			default:
				$description = '';
		}

		print<<<HERE
		<!DOCTYPE html>
		<html lang="ja">
		<head>
			<title>$pageTitle | 中古トラックのワンプラストア</title>
			<meta charset="UTF-8">
			$description
			<meta property="og:title" content="ウイング車  | 中古トラックのワンプラストア"/>
			<meta property="og:url" content="https://www.truc-net.jp"/>
			<meta property="og:type" content="website"/>
			<meta property="og:site_name" content="中古トラックのワンプラストア"/>
			<meta property="og:image" content="https://www.truc-net.jp"/>
			<meta property="og:image:width" content="1200"/>
			<meta property="og:image:height" content="630"/>
			<meta name="viewport" content="width=device-width"/>
			<meta name="format-detection" content="telephone=no"/>
			<link rel="shortcut icon" href="/images/common/favicon.ico">
			<link rel="stylesheet" type="text/css" href="/css/style.css?ver=180817">

			<script src="/js/jquery.js"></script>
			<script src="/js/slick.min.js"></script>
			<script src="/js/script.js?ver=180817"></script>

			<!--[if lt IE 9]>
			<script src="/js/html5shiv.js" type="text/javascript"></script>
			<script src="/js/respond.min.js" type="text/javascript"></script>
			<![endif]-->

			<!-- Google Tag Manager for Kuriyama Office -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-PVBWR6X');</script>
			<!-- End Google Tag Manager -->

			<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-TDKMK87');</script>
			<!-- End Google Tag Manager -->
		</head>
		<body>
		<!-- Google Tag Manager (noscript) for Kuriyama Office -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PVBWR6X"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDKMK87"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
			<header class="header_top">
				<!--<div class="header_top_image">
					<img src="/images/common/top.jpg" alt="">
				</div>-->
				<div class="header_top_logo_area">
					<div class="wrap">
						<div class="header_top_caption">
							<img src="/images/common/top_caption.png" alt="">
						</div>
						<div class="header_top_logo">
							<img src="/images/common/logo.png" alt="">
						</div>
					</div>
				</div>
	      <div class="header_top_video">
	        <video autoplay loop muted playsinline>
						<source src="/videos/wanpura.mp4" type="video/mp4" media="(min-width: 768px)">
						<source src="/videos/wanpura_mobile.mp4" media="(max-width: 767px)">
	        </video>
	      </div>
					<nav class="nav_sp">
						<ul>
							<li>
								<div id="burger">
									<svg xmlns="http://www.w3.org/2000/svg" width="20" height="14.5" viewBox="0 0 20 14.5">
										<defs><style>.burger {fill: #fff;}</style></defs>
										<rect class="burger" width="20" height="2.5"/>
										<rect class="burger" y="6" width="20" height="2.5"/>
										<rect class="burger" y="12" width="20" height="2.5"/>
									</svg>
									<span>メニュー</span>
								</div>
							</li>
							<li>
								<a href="/"><span>ホーム</span></a>
							</li>
							<li>
								<a href="/trucks"><span>トラック一覧</span></a>
							</li>
							<li class="toggle_icon">
								<a dom="toggle_contact_sp"><span>問合せ</span></a>
							</li>
						</ul>
					</nav>
					<nav class="contact_sp">
						<h1 class="contact_sp_title" dom="close_contact_sp">お問い合わせは、お気軽に！</h1>
						<dl class="contact_btn">
							<dt>▼ お電話から</dt>
							<dd>
								<a href="tel:08041706759">お問合せは小佐田(こさだ)まで<hr><span class="tel_number">080-4170-6759</span><span class="time_info">9:00〜18:00 (日・祝休み) </span></a>
								</dd>
						</dl>
						<dl class="contact_btn">
							<dt>▼ フォームから</dt>
							<dd class="contact_btn_form">
								<a href="/contact"><span>1分でカンタン入力！</span></a>
							</dd>
						</dl>
				  </nav>
				</div>
			</header>
		<nav id="nav">
			<div dom="buger_nav_base" class="buger_nav_base"></div>
			<div class="nav_wrap">
				<div id="close">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="22.3" viewBox="0 0 16 22.3">
						<defs><style>.a, .b {fill: #fff;}.b {fill-rule: evenodd;}</style></defs>
						<polygon class="b" points="2 0 16 14 14.5 16 0 1.5 2 0"/>
						<polygon class="b" points="16 2 2 16 0 14.5 14.5 0 16 2"/>
					</svg>
				</div>
				<ul class="nav_btn">
					<li id="inqury"><a href="/contact"><img src=" https://www.truskey.jp/images/common/bg_inqury.svg" alt=""><span>お問合わせ</span></a></li>
				</ul>
				<ul class="nav_main" dom="nav_main">
					<li>
						<a href="/"><span>トップページ</span></a>
					</li>
					<li>
						<a href="/trucks"><span>トラックを探す</span></a>
					</li>
					<li>
						<a href="/guide"><span>ご購入の流れ</span></a>
					</li>
					<li>
						<a href="/faq"><span>よくあるご質問</span></a>
					</li>
					<li>
						<a href="/contact"><span>お問い合わせ</span></a>
					</li>
					<li>
						<a href="/company"><span>会社案内</span></a>
					</li>
				</ul>
			</div>
		</nav>
		<!-- start main -->
		<div class="wrap">
			$content

	    <div class="banners_yahooauction">
	      <a href="https://auctions.yahoo.co.jp/jp/show/rating?userID=kuriyamaauto_japan" target="_blank"><img src="/images/banner/yahooauction.jpg"></a>
	      <p class="satisfaction">顧客満足率 <span>98.3%</span></p>
	      <p class="satisfaction_text">全レビュー119件のうち、117件のお客様に高評価を頂いております。</p>
	      <p class="satisfaction_notice">※2018年10月03日現在</p>
				<p class="satisfaction_image"><img src="/images/banner/yahooauction2018_12.jpg" alt="ヤフオク出品マスター ダイヤモンド"></p>
	    </div>
HERE;
	require $_SERVER['DOCUMENT_ROOT']. "/settings/search_icon.php";
	print($search_icon);
	require $_SERVER['DOCUMENT_ROOT']. "/settings/contact.php";
	print('<h2 class="title">お問い合わせ</h2><div style="font-size: 121%; border: 3px solid #999; margin: 0px auto 20px auto; padding:0; text-align: left; line-height:150%;">
  			<h4 style="background: #999; color: #FFF; padding: 2px 0 2px 5px;">冬季休業日程のおしらせ</h4>
  			<p style="background: #FFE; padding: 2px 0 2px 5px; color: #000;">
  				12/29(土)～1/6(日)までの期間、冬季休業とさせていただきます。
  			</p>
  		</div>');
	print($contact);
	print<<<HERE
		</div>
		<!-- end main -->
		<footer class="footer">
			<div class="footer_in">
				<div class="wrap">
					<nav class="nav_footer">
						<ul class="footer_tree_logo">
							<li><a href="/"><img src="/images/common/logo.png" alt="中古トラックのワンプラストア"></a></li>
						</ul>
						<dl class="footer_treetop">
							<dt><a href="/trucks">中古トラック</a></dt>
							<dd>
								<ul>
									<li>
										<a href="/trucks">在庫一覧</a>
									</li>
									<li>
										<a href="/guide">ご購入の流れ</a>
									</li>
									<li>
										<a href="/faq">よくあるご質問</a>
									</li>
								</ul>
								<dl class="footer_inner_list">
									<dt>タイプ</dt>
									<dd><a href="/trucks/?cat=1,2">ウイング・バン</a></dd>
									<dd><a href="/trucks/?cat=3">冷凍ウイング・バン</a></dd>
									<dd><a href="/trucks/?cat=4,5,6">平・クレーン・ダンプ</a></dd>
									<dd><a href="/trucks/?cat=7">トラクタ・トレーラー</a></dd>
									<dd><a href="/trucks/?cat=8,9,10,11">その他</a></dd>
								</dl>
								<ul class="footer_inner_add">
									<li>
										<a href="https://auctions.yahoo.co.jp/seller/kuriyamaauto_japan" target="_blank">Yahoo!オークション出品中</a>
									</li>
								</ul>
							</dd>
						</dl>
						<dl class="footer_treetop">
							<dt><a href="https://www.kuriyama-truck.com/parts" target="_blank">リサイクルパーツ<span>( 栗山自動車サイト )</span></a></dt>
							<dd>
								<ul>
									<li>
										<a href="https://www.kuriyama-truck.com/list_parts?page=1&sort=0&rows=50&category=1&part=エンジン" target="_blank">エンジン</a>
									</li>
									<li>
										<a href="https://www.kuriyama-truck.com/list_parts?page=1&sort=0&rows=50&category=2&part=ミッション" target="_blank">ミッション</a>
									</li>
									<li>
										<a href="https://www.kuriyama-truck.com/list_parts?page=1&sort=0&rows=50&category=3&part=デフ" target="_blank">デフ</a>
									</li>
									<li>
										<a href="https://www.kuriyama-truck.com/list_parts?page=1&sort=0&rows=50&category=4&part=キャビン" target="_blank">キャビン</a>
									</li>
									<li>
										<a href="https://www.kuriyama-truck.com/list_parts?page=1&sort=0&rows=50&category=6&part=電装品" target="_blank">電装品</a>
									</li>
									<li>
										<a href="https://www.kuriyama-truck.com/list_parts?page=1&sort=0&rows=50&category=5&part=外装品" target="_blank">外装品</a>
									</li>
									<li>
										<a href="https://www.kuriyama-truck.com/list_parts?page=1&sort=0&rows=50&category=7&part=ボディ関連" target="_blank">ボディ関連</a>
									</li>
									<li>
										<a href="https://www.kuriyama-truck.com/list_parts?page=1&sort=0&rows=50&category=8&part=その他" target="_blank">その他</a>
									</li>
								</ul>
							</dd>
						</dl>
						<dl class="footer_treetop">
							<dt><a href="/company">会社案内</a></dt>
							<dd>
								<ul>
									<li>
										<a href="/company">会社概要</a>
									</li>
								</ul>
								<dl class="footer_inner_list">
									<dt>栗山自動車グループ</dt>
									<dd><a href="https://www.kuriyama-truck.com/" target="_blank">栗山自動車工業</a></dd>
									<dd><a href="https://www.truskey.jp/" target="_blank">トラスキー</a></dd>
									<dd><a href="http://www.truckcom.co.jp/" target="_blank">トラックコム(西日本)</a></dd>
									<dd><a href="http://www.kuriyama-motors.jp/" target="_blank">KURIYAMA MOTORS(海外)</a></dd>
								</dl>
							</dd>
						</dl>
						<dl class="footer_treetop">
							<dt><a href="/contact">お問い合わせ</a></dt>
							<dd>
								<ul>
									<li>
										<a href="/contact">全般的なお問い合わせ</a>
									</li>
								</ul>
							</dd>
						</dl>
					</nav>
					<ul class="other">
						<li><a href="/personal_info/">個人情報保護方針</a></li>
						<li><a href="/law/">特商法に基づく表示</a></li>
						<li><a href="/company/#navi1">古物法に基づく表示</a></li>
					</ul>
				</div>
			</div>
      <div class="modal" dom="modal">
        <button class="button_close"><span>閉じる</span></button>
        <div class="modal_add_line">
          <p>「友だち追加」から、QRコードまたはID検索の電話番号入力で登録してください。</p>
          <img src="/images/common/add_line.png" alt="LINEアカウントの追加方法">
        </div>
      </div>

			<div class="copyright">Copyright (C) ワンプラストア , All rights reserved.</div>
		</footer>
		</body>
		</html>
HERE;
	}

}


?>
