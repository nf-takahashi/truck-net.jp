<?php

//変数の宣言
$search_icon =<<< HERE
<div class="search_icon">
	<h2 class="title_search"><span>上物形状</span>で探す</h2>
	<ul class="search_icon_list">
		<li>
			<a href='/trucks/?cat=1,2'>
				<div class="icon"><img src="/images/icon/icon01.png" alt=""></div>
				<div class="search_icon_text">ウイング<br>バン</div>
			</a>
		</li>
		<li>
			<a href='/trucks/?cat=3'>
				<div class="icon"><img src="/images/icon/icon23.png" alt=""></div>
				<div class="search_icon_text">冷蔵冷凍車</div>
			</a>
		</li>
		<li>
			<a href='/trucks/?cat=4,5,6'>
				<div class="icon"><img src="/images/icon/icon04.png" alt=""></div>
				<div class="search_icon_text">平・クレーン<br>ダンプ</div>
			</a>
		</li>
		<li>
			<a href='/trucks/?cat=7'>
				<div class="icon"><img src="/images/icon/icon14.png" alt=""></div>
				<div class="search_icon_text">トラクター<br>トレーラー</div>
			</a>
		</li>
		<li>
			<a href='/trucks/?cat=8,9,10,11'>
				<div class="icon"><img src="/images/icon/icon17.png" alt=""></div>
				<div class="search_icon_text">その他</div>
			</a>
		</li>
		<li>
			<a href='/trucks/?cat='>
				<div class="icon"><img src="/images/icon/icon18.png" alt=""></div>
				<div class="search_icon_text">すべて</div>
			</a>
		</li>
	</ul>
</div>
HERE;

?>
