<?php
switch($value['ec_site_1']){
    case 0: $ec_site_1 = "-"; break;
    case 1: $ec_site_1 = "はい"; break;
}
switch($value['ec_site_2']){
    case 0: $ec_site_2 = "-"; break;
    case 1: $ec_site_2 = "はい"; break;
}
switch($value['ec_site_3']){
    case 0: $ec_site_3 = "-"; break;
    case 1: $ec_site_3 = "はい"; break;
}
switch($value['ec_site_4']){
    case 0: $ec_site_4 = "-"; break;
    case 1: $ec_site_4 = "はい"; break;
}
switch($value['manufacturer']){
    case 0: $manufacturer = "なし"; break;
    case 1: $manufacturer = "日野"; break;
    case 2: $manufacturer = "いすず"; break;
    case 3: $manufacturer = "三菱ふそう"; break;
    case 4: $manufacturer = "日産UD"; break;
    case 5: $manufacturer = "トヨタ"; break;
    case 6: $manufacturer = "マツダ"; break;
    case 7: $manufacturer = "その他"; break;
    case 8: $manufacturer = "ボルボ"; break;
    case 9: $manufacturer = "ベンツ"; break;
}
switch($value['veh_cat']){
    case 0: $veh_cat = "選択なし"; break;
    case 1: $veh_cat = "２トン"; break;
    case 2: $veh_cat = "４トン"; break;
    case 3: $veh_cat = "増トン"; break;
    case 4: $veh_cat = "大型"; break;
    case 5: $veh_cat = "その他"; break;
}
switch($value['drive']){
    case 0: $drive = "-"; break;
    case 1: $drive = "4×2"; break;
    case 2: $drive = "4×4"; break;
    case 3: $drive = "6×2"; break;
    case 4: $drive = "6×4"; break;
    case 5: $drive = "6×6"; break;
    case 6: $drive = "前2軸"; break;
    case 7: $drive = "8×4"; break;
}
switch($value['suspension']){
    case 0: $suspension = "-"; break;
    case 1: $suspension = "リーフサス"; break;
    case 2: $suspension = "リアエアサス"; break;
    case 3: $suspension = "総輪エアサス"; break;
    case 4: $suspension = "F独立懸架"; break;
}
switch($value['shape']){
    case 0: $shape = "なし"; break;
    case 1: $shape = "ウイング"; break;
    case 2: $shape = "バン"; break;
    case 3: $shape = "冷凍ウイング・バン"; break;
    case 4: $shape = "クレーン・セルフ"; break;
    case 5: $shape = "平ボディー"; break;
    case 6: $shape = "ダンプ"; break;
    case 7: $shape = "トラクタ・トレーラー"; break;
    case 8: $shape = "パッカー・清掃関係"; break;
    case 9: $shape = "アームロール"; break;
    case 10: $shape = "ミキサー車"; break;
    case 11: $shape = "その他"; break;
}
switch($value['power_gate']){
    case 0: $power_gate = "-"; break;
    case 1: $power_gate = "無"; break;
    case 2: $power_gate = "かぶせ"; break;
    case 3: $power_gate = "跳上"; break;
    case 4: $power_gate = "格納"; break;
    case 5: $power_gate = "垂直"; break;
    case 6: $power_gate = "引出"; break;
}
switch($value['body_width']){
    case 0: $body_width = "選択なし"; break;
    case 1: $body_width = "ワイド"; break;
    case 2: $body_width = "セミワイド"; break;
    case 3: $body_width = "標準幅"; break;
    case 4: $body_width = "大型"; break;
}
switch($value['roof']){
    case 0: $roof = "-"; break;
    case 2: $roof = "ロールーフ"; break;
    case 1: $roof = "ハイルーフ"; break;
    case 3: $roof = "スーパーハイルーフ"; break;
}
switch($value['bed']){
    case 0: $bed = "-"; break;
    case 1: $bed = "ベッド付"; break;
    case 2: $bed = "ベッドレス"; break;
    case 3: $bed = "スーパーハイ"; break;
}
switch($value['shifls']){
    case 0: $shifts = "-"; break;
    case 1: $shifts = "4MT"; break;
    case 2: $shifts = "5MT"; break;
    case 3: $shifts = "6MT"; break;
    case 4: $shifts = "7MT"; break;
    case 5: $shifts = "スムーサー(2ペダル)"; break;
    case 6: $shifts = "スムーサー(3ペダル)"; break;
    case 7: $shifts = "プロシフト(2ペダル)"; break;
    case 8: $shifts = "プロシフト(3ペダル)"; break;
    case 9: $shifts = "イノマット(2ペダル)"; break;
    case 10: $shifts = "イノマット(3ペダル)"; break;
    case 11: $shifts = "エスコット(2ペダル)"; break;
    case 12: $shifts = "エスコット(3ペダル)"; break;
    case 13: $shifts = "5速ハイロー"; break;
    case 14: $shifts = "6速ハイロー"; break;
    case 15: $shifts = "8速ハイロー"; break;
    case 16: $shifts = "セミAT(2ペダル)"; break;
    case 17: $shifts = "セミAT(3ペダル)"; break;
    case 18: $shifts = "オートマ"; break;
    case 19: $shifts = "8MT"; break;
    case 20: $shifts = "9MT"; break;
}
switch($value['inspect_any']){
    case 0: $inspect_any = "-"; break;
    case 1: $inspect_any = "一時抹消"; break;
    case 2: $inspect_any = "一時抹消　車検切れ"; break;
    case 3: $inspect_any = "一時抹消　予備検あり"; break;
    case 4: $inspect_any = "予備検査取得予定"; break;
    case 5: $inspect_any = "車検取得予定"; break;
    case 6: $inspect_any = "車検切れ ナンバー付き"; break;
}
switch($value['vehicle_gross_weight']){
    case 0: $vehicle_gross_weight = "-"; break;
    case 1: $vehicle_gross_weight = "1ｔ以下"; break;
    case 2: $vehicle_gross_weight = "2ｔ以下"; break;
    case 3: $vehicle_gross_weight = "3ｔ以下"; break;
    case 4: $vehicle_gross_weight = "4ｔ以下"; break;
    case 5: $vehicle_gross_weight = "5ｔ以下"; break;
    case 6: $vehicle_gross_weight = "6ｔ以下"; break;
    case 7: $vehicle_gross_weight = "7ｔ以下"; break;
    case 8: $vehicle_gross_weight = "8ｔ以下"; break;
    case 9: $vehicle_gross_weight = "9ｔ以下"; break;
    case 10: $vehicle_gross_weight = "10ｔ以下"; break;
    case 11: $vehicle_gross_weight = "11ｔ以下"; break;
    case 12: $vehicle_gross_weight = "12ｔ以下"; break;
    case 13: $vehicle_gross_weight = "13ｔ以下"; break;
    case 14: $vehicle_gross_weight = "14ｔ以下"; break;
    case 15: $vehicle_gross_weight = "15ｔ以下"; break;
    case 16: $vehicle_gross_weight = "16ｔ以下"; break;
    case 17: $vehicle_gross_weight = "17ｔ以下"; break;
    case 18: $vehicle_gross_weight = "18ｔ以下"; break;
    case 19: $vehicle_gross_weight = "19ｔ以下"; break;
    case 20: $vehicle_gross_weight = "20ｔ以下"; break;
    case 21: $vehicle_gross_weight = "21ｔ以下"; break;
    case 22: $vehicle_gross_weight = "22ｔ以下"; break;
    case 23: $vehicle_gross_weight = "23ｔ以下"; break;
    case 24: $vehicle_gross_weight = "24ｔ以下"; break;
    case 25: $vehicle_gross_weight = "25ｔ以下"; break;
    case 26: $vehicle_gross_weight = "26ｔ以下"; break;
    case 27: $vehicle_gross_weight = "27ｔ以下"; break;
    case 28: $vehicle_gross_weight = "28ｔ以下"; break;
    case 29: $vehicle_gross_weight = "29ｔ以下"; break;
    case 30: $vehicle_gross_weight = "30ｔ以下"; break;
}
switch($value['new_ordinary_license_conformity']){
    case 0: $new_ordinary_license_conformity = "-"; break;
    case 1: $new_ordinary_license_conformity = "新普通免許適合"; break;
    case 2: $new_ordinary_license_conformity = "新中型免許適合"; break;
    case 3: $new_ordinary_license_conformity = "大型免許"; break;
}
switch($value['fuel']){
    case 0: $fuel = "-"; break;
    case 1: $fuel = "軽油"; break;
    case 2: $fuel = "ガソリン"; break;
    case 3: $fuel = "CNG"; break;
    case 4: $fuel = "LPG"; break;
    case 5: $fuel = "ハイブリッド"; break;
    case 6: $fuel = "電池"; break;
}
switch($value['engine_model']){
    case 0: $engine_model = "-"; break;
    case 24: $engine_model = "フルハーフ"; break;
    case 25: $engine_model = "トランテックス"; break;
    case 26: $engine_model = "パブコ"; break;
    case 27: $engine_model = "トレクス"; break;
    case 28: $engine_model = "キタムラ"; break;
    case 29: $engine_model = "日野車体"; break;
    case 30: $engine_model = "トーヨー"; break;
    case 31: $engine_model = "東プレ"; break;
    case 32: $engine_model = "花見台"; break;
    case 33: $engine_model = "四国車体"; break;
    case 34: $engine_model = "コダイラ"; break;
    case 35: $engine_model = "イカリヤ"; break;
    case 36: $engine_model = "新明和"; break;
    case 37: $engine_model = "各和"; break;
    case 38: $engine_model = "ＹＯＫＯＳＨＡ"; break;
    case 39: $engine_model = "富士車輌"; break;
    case 40: $engine_model = "モリタエコノス"; break;
    case 41: $engine_model = "カヤバ"; break;
    case 42: $engine_model = "極東"; break;
    case 43: $engine_model = "その他"; break;
}
switch($value['mitigation']){
    case 0: $mitigation = "-"; break;
    case 1: $mitigation = "なし"; break;
    case 2: $mitigation = "一括緩和"; break;
    case 3: $mitigation = "海コン一括"; break;
    case 4: $mitigation = "バラ緩和"; break;
}
switch($value['floor']){
    case 0: $floor = "-"; break;
    case 1: $floor = "木"; break;
    case 2: $floor = "鉄"; break;
    case 3: $floor = "ｾﾝﾀｰﾛｰﾗｰ"; break;
    case 4: $floor = "アルミ"; break;
    case 5: $floor = "キーストン"; break;
    case 6: $floor = "ステン"; break;
    case 7: $floor = "その他"; break;
}
switch($value['tilt']){
    case 0: $tilt = "-"; break;
    case 1: $tilt = "アルミブロック"; break;
    case 2: $tilt = "木平"; break;
}
switch($value['opening_direction']){
    case 0: $opening_direction = "-"; break;
    case 1: $opening_direction = "７"; break;
    case 2: $opening_direction = "５"; break;
    case 3: $opening_direction = "３"; break;
}
switch($value['radio_control']){
    case 0: $radio_control = "-"; break;
    case 1: $radio_control = "なし"; break;
    case 2: $radio_control = "有"; break;
}
switch($value['winch']){
    case 0: $winch = "-"; break;
    case 1: $winch = "なし"; break;
    case 2: $winch = "有"; break;
}
switch($value['auto_ayumi_plate']){
    case 0: $auto_ayumi_plate = "-"; break;
    case 1: $auto_ayumi_plate = "なし"; break;
    case 2: $auto_ayumi_plate = "有"; break;
}
switch($value['hoist']){
    case 0: $hoist = "-"; break;
    case 1: $hoist = "シングル"; break;
    case 2: $hoist = "ツイン"; break;
}
switch($value['koboren']){
    case 0: $koboren = "-"; break;
    case 1: $koboren = "なし"; break;
    case 2: $koboren = "手動"; break;
    case 3: $koboren = "電動"; break;
}
switch($value['own_weight_meter']){
    case 0: $own_weight_meter = "-"; break;
    case 1: $own_weight_meter = "なし"; break;
    case 2: $own_weight_meter = "有"; break;
}
switch($value['put_in_method']){
    case 0: $put_in_method = "-"; break;
    case 1: $put_in_method = "プレス式"; break;
    case 2: $put_in_method = "巻込み式"; break;
}
switch($value['freezing_machine_manufacturer']){
    case 0: $freezing_machine_manufacturer = "-"; break;
    case 58: $freezing_machine_manufacturer = "菱重"; break;
    case 59: $freezing_machine_manufacturer = "サーモキング"; break;
    case 60: $freezing_machine_manufacturer = "東芝"; break;
    case 61: $freezing_machine_manufacturer = "その他"; break;
}
switch($value['freezing_machine_standby']){
    case 0: $freezing_machine_standby = "-"; break;
    case 1: $freezing_machine_standby = "なし"; break;
    case 2: $freezing_machine_standby = "有(コード有)"; break;
    case 3: $freezing_machine_standby = "有(コード欠)"; break;
}
switch($value['freezing_inspecting_flag']){
    case 0: $freezing_inspecting_flag = "-"; break;
    case 1: $freezing_inspecting_flag = "はい"; break;
}
switch($value['packer_fuel']){
    case 0: $packer_fuel = "-"; break;
    case 1: $packer_fuel = "LPG"; break;
    case 2: $packer_fuel = "CNG"; break;
    case 3: $packer_fuel = "ディーゼル"; break;
}
switch($value['vertical_frame']){
    case 0: $vertical_frame = "-"; break;
    case 1: $vertical_frame = "木"; break;
    case 2: $vertical_frame = "鉄"; break;
    case 3: $vertical_frame = "アルミ"; break;
}
switch($value['horizontal_frame']){
    case 0: $horizontal_frame = "-"; break;
    case 1: $horizontal_frame = "木"; break;
    case 2: $horizontal_frame = "鉄"; break;
    case 3: $horizontal_frame = "アルミ"; break;
}
switch($value['continuous_switch']){
    case 0: $continuous_switch = "-"; break;
    case 1: $continuous_switch = "なし"; break;
    case 2: $continuous_switch = "有"; break;
}
switch($value['gate_manufacturer']){
    case 0: $gate_manufacturer = "-"; break;
    case 44: $gate_manufacturer = "新明和"; break;
    case 45: $gate_manufacturer = "極東"; break;
    case 46: $gate_manufacturer = "フルハーフ"; break;
    case 47: $gate_manufacturer = "パブコ"; break;
    case 48: $gate_manufacturer = "トレクス"; break;
    case 49: $gate_manufacturer = "キタムラ"; break;
    case 50: $gate_manufacturer = "コダイラ"; break;
    case 51: $gate_manufacturer = "日野車体"; break;
    case 52: $gate_manufacturer = "その他"; break;
}
switch($value['open_and_close']){
    case 0: $open_and_close = "-"; break;
    case 1: $open_and_close = "オートターン"; break;
    case 2: $open_and_close = "バランスターン(手動)"; break;
}
switch($value['stopper']){
    case 0: $stopper = "-"; break;
    case 1: $stopper = "なし"; break;
    case 2: $stopper = "有"; break;
    case 3: $stopper = "有(分割可)"; break;
}
switch($value['crane_manufacturer']){
    case 0: $crane_manufacturer = "-"; break;
    case 53: $crane_manufacturer = "古河ユニック"; break;
    case 54: $crane_manufacturer = "タダノ"; break;
    case 55: $crane_manufacturer = "カトウ"; break;
    case 56: $crane_manufacturer = "ヒアブ"; break;
    case 57: $crane_manufacturer = "その他"; break;
}
switch($value['crane_radio_control']){
    case 0: $crane_radio_control = "-"; break;
    case 1: $crane_radio_control = "なし"; break;
    case 2: $crane_radio_control = "有"; break;
}
switch($value['hook_in']){
    case 0: $hook_in = "-"; break;
    case 1: $hook_in = "なし"; break;
    case 2: $hook_in = "有"; break;
}
switch($value['boom_stage_number']){
    case 0: $boom_stage_number = "-"; break;
    case 1: $boom_stage_number = "3"; break;
    case 2: $boom_stage_number = "4"; break;
    case 3: $boom_stage_number = "5"; break;
    case 4: $boom_stage_number = "6"; break;
}
switch($value['hanging_tonnage']){
    case 0: $hanging_tonnage = "-"; break;
    case 1: $hanging_tonnage = "2.63"; break;
    case 2: $hanging_tonnage = "2.93"; break;
}
switch($value['insert_diff_outrigger']){
    case 0: $insert_diff_outrigger = "-"; break;
    case 1: $insert_diff_outrigger = "なし"; break;
    case 2: $insert_diff_outrigger = "有"; break;
}
switch($value['high_jack']){
    case 0: $high_jack = "-"; break;
    case 1: $high_jack = "なし"; break;
    case 2: $high_jack = "有"; break;
}
switch($value['rear_outrigger']){
    case 0: $rear_outrigger = "-"; break;
    case 1: $rear_outrigger = "なし"; break;
    case 2: $rear_outrigger = "有"; break;
}
switch($value['back_eye']){
    case 0: $back_eye = "-"; break;
    case 1: $back_eye = "なし"; break;
    case 2: $back_eye = "有"; break;
    case 3: $back_eye = "カラー"; break;
}
switch($value['discharge_lights']){
    case 0: $discharge_lights = "-"; break;
    case 1: $discharge_lights = "有"; break;
    case 2: $discharge_lights = "無"; break;
}
switch($value['retarder']){
    case 0: $retarder = "-"; break;
    case 1: $retarder = "エンジン"; break;
    case 2: $retarder = "ミッション"; break;
}
switch($value['radio']){
    case 0: $radio = "-"; break;
    case 1: $radio = "有"; break;
    case 2: $radio = "無"; break;
}
switch($value['tarbo']){
    case 0: $tarbo = "-"; break;
    case 1: $tarbo = "有"; break;
    case 2: $tarbo = "無"; break;
}
switch($value['hill_holder_device']){
    case 0: $hill_holder_device = "-"; break;
    case 1: $hill_holder_device = "有"; break;
    case 2: $hill_holder_device = "無"; break;
}
switch($value['pm_muffler']){
    case 0: $pm_muffler = "-"; break;
    case 1: $pm_muffler = "なし"; break;
    case 2: $pm_muffler = "なし(装置なしで走行可)"; break;
    case 3: $pm_muffler = "有"; break;
    case 4: $pm_muffler = "有(証明書有)"; break;
}
switch($value['etc']){
    case 0: $etc = "-"; break;
    case 1: $etc = "有"; break;
    case 2: $etc = "無"; break;
}
switch($value['alloy_wheels']){
    case 0: $alloy_wheels = "-"; break;
    case 1: $alloy_wheels = "有"; break;
    case 2: $alloy_wheels = "無"; break;
}
switch($value['presenocation']){
    case 0: $presenocation = "-"; break;
    case 1: $presenocation = "客先"; break;
    case 2: $presenocation = "仕入先"; break;
    case 3: $presenocation = "千葉支店"; break;
    case 4: $presenocation = "千葉 第1P"; break;
    case 5: $presenocation = "千葉 第2P"; break;
    case 6: $presenocation = "千葉 第3P"; break;
    case 7: $presenocation = "千葉 第4P"; break;
    case 8: $presenocation = "神奈川支店"; break;
    case 9: $presenocation = "東京営業所"; break;
    case 10: $presenocation = "陸送機構 置場"; break;
    case 11: $presenocation = "栄自動車"; break;
    case 12: $presenocation = "平野自動車"; break;
    case 13: $presenocation = "濱野自動車"; break;
    case 14: $presenocation = "ディーラー"; break;
    case 15: $presenocation = "AA アライ小山"; break;
    case 16: $presenocation = "AA アライベイ"; break;
    case 17: $presenocation = "AA U-MAX幕張"; break;
    case 18: $presenocation = "AA U-MAX神戸"; break;
    case 19: $presenocation = "AA その他AA・入札会"; break;
    case 20: $presenocation = "トラックコム"; break;
    case 21: $presenocation = "サンフェニ柏"; break;
    case 22: $presenocation = "サンフェニ浮島"; break;
    case 23: $presenocation = "海上輸送中"; break;
    case 24: $presenocation = "ジュベルアリ港"; break;
    case 25: $presenocation = "コンテナヤード"; break;
    case 26: $presenocation = "ショールーム"; break;
    case 27: $presenocation = "千葉 第5P"; break;
    case 28: $presenocation = "千葉 第6P"; break;
}
$app_price_any = $value['app_price_any']? $value['app_price_any']. '' : '';
$global_price = $value['global_price']? $value['global_price']. '' : '';
$length = $value['length']? $value['length']. 'cm' : '';
$width = $value['width']? $value['width']. 'cm' : '';
$height = $value['height']? $value['height']. 'cm' : '';
$id = $value['id']? $value['id']. '' : '';
$nego_status = $value['nego_status']? $value['nego_status']. '' : '';
$model_year = $value['model_year']? $value['model_year']. '' : '';
$model_year_month = $value['model_year_month']? $value['model_year_month']. '月' : '';
$model = $value['model']? $value['model']. '' : '';
$vehicle_no = $value['vehicle_no']? $value['vehicle_no']. '' : '';
$mileage = $value['mileage']>0? sprintf('%.1f',($value['mileage'] / 10))."万km": "-";
$inspect_year = $value['inspect_year']? $value['inspect_year']. '' : '';
$inspect_month = $value['inspect_month']? $value['inspect_month']. '月' : '';
$inspect_day = $value['inspect_day']? $value['inspect_day']. '日' : '';
$max_load_cap = $value['max_load_cap']? $value['max_load_cap']. 'kg' : '';
$horse_power = $value['horse_power']? $value['horse_power']. 'ps' : '';
$capacity = $value['capacity']? $value['capacity']. '人' : '';
$propulsion_notices = $value['propulsion_notices']? $value['propulsion_notices']. '' : '';
$selling_title = $value['selling_title']? $value['selling_title']. '' : '';
$chassis_info = $value['chassis_info']? $value['chassis_info']. '' : '';
$remarks = $value['remarks']? $value['remarks']. '' : '';
$body_manufacturer = $value['body_manufacturer']? $value['body_manufacturer']. '' : '';
$body_model = $value['body_model']? $value['body_model']. '' : '';
$lashing = $value['lashing']? $value['lashing']. '段' : '';
$inner_hook = $value['inner_hook']? $value['inner_hook']. '対' : '';
$rope_hole = $value['rope_hole']? $value['rope_hole']. '対' : '';
$joloda_rail = $value['joloda_rail']? $value['joloda_rail']. '列' : '';
$stanchion_hall = $value['stanchion_hall']? $value['stanchion_hall']. '個' : '';
$container_manufacturer = $value['container_manufacturer']? $value['container_manufacturer']. '' : '';
$tank_inspect_deadline_year = $value['tank_inspect_deadline_year']? $value['tank_inspect_deadline_year']. '年' : '';
$tank_inspect_deadline_month = $value['tank_inspect_deadline_month']? $value['tank_inspect_deadline_month']. '月' : '';
$tank_inspect_deadline_day = $value['tank_inspect_deadline_day']? $value['tank_inspect_deadline_day']. '日' : '';
$freezing_machine_model = $value['freezing_machine_model']? $value['freezing_machine_model']. '' : '';
$freezing_preset_temperature = $value['freezing_preset_temperature']? "-". $value['freezing_preset_temperature']. '度' : '';
$freezing_before_operate_temperature = $value['freezing_before_operate_temperature']? $value['freezing_before_operate_temperature']. '度' : '';
$freezing_measurement_time = $value['freezing_measurement_time']? $value['freezing_measurement_time']. '時間' : '';
$freezing_measurement_temprature = $value['freezing_measurement_temprature']? $value['freezing_measurement_temprature']. '度' : '';
$door_width = $value['door_width']? $value['door_width']. 'cm' : '';
$door_height = $value['door_height']? $value['door_height']. 'cm' : '';
$side_door_width = $value['side_door_width']? $value['side_door_width']. 'cm' : '';
$side_door_height = $value['side_door_height']? $value['side_door_height']. 'cm' : '';
$wall_thickness_side = $value['wall_thickness_side']? $value['wall_thickness_side']. 'cm' : '';
$wall_thickness_front = $value['wall_thickness_front']? $value['wall_thickness_front']. 'cm' : '';
$wall_thickness_door = $value['wall_thickness_door']? $value['wall_thickness_door']. 'cm' : '';
$volume = $value['volume']? $value['volume']. '立米' : '';
$drum_type = $value['drum_type']? $value['drum_type']. '' : '';
$rear_tilt_height = $value['rear_tilt_height']? $value['rear_tilt_height']. 'cm' : '';
$road_plate = $value['road_plate']? $value['road_plate']. 'cm' : '';
$ground_height = $value['ground_height']? $value['ground_height']. 'cm' : '';
$ground_clearance_front_min = $value['ground_clearance_front_min']? $value['ground_clearance_front_min']. 'cm' : '';
$ground_clearance_rear_min = $value['ground_clearance_rear_min']? $value['ground_clearance_rear_min']. 'cm' : '';
$horizontal_frame_number = $value['horizontal_frame_number']? $value['horizontal_frame_number']. '本' : '';
$expiration_year = $value['expiration_year']? $value['expiration_year']. '年' : '';
$expiration_month = $value['expiration_month']? $value['expiration_month']. '月' : '';
$expiration_day = $value['expiration_day']? $value['expiration_day']. '' : '日';
$container_model = $value['container_model']? $value['container_model']. '' : '';
$maximum_mixing_capacity = $value['maximum_mixing_capacity']? $value['maximum_mixing_capacity']. '' : '';
$drum_length = $value['drum_length']? $value['drum_length']. 'm' : '';
$gate_model = $value['gate_model']? $value['gate_model']. '' : '';
$gate_updown_power = $value['gate_updown_power']? $value['gate_updown_power']. 'kg' : '';
$measure_depth = $value['measure_depth']? $value['measure_depth']. 'cm' : '';
$measure_width = $value['measure_width']? $value['measure_width']. 'cm' : '';
$depth_to_stopper = $value['depth_to_stopper']? $value['depth_to_stopper']. 'cm' : '';
$width_to_stopper = $value['width_to_stopper']? $value['width_to_stopper']. 'cm' : '';
$crane_model = $value['crane_model']? $value['crane_model']. '' : '';
$crane_manufacturing_year = $value['crane_manufacturing_year']? $value['crane_manufacturing_year']. '年' : '';
$crane_manufacturing_month = $value['crane_manufacturing_month']? $value['crane_manufacturing_month']. '月' : '';
$fuel_tank = $value['fuel_tank']? $value['fuel_tank']. 'L' : '';
$recycle_ticket = $value['recycle_ticket']? $value['recycle_ticket']. '円' : '';
$selling_price = $value['selling_price']? $value['selling_price']. '' : '';
$tax_inc_selling_price = $value['tax_inc_selling_price']? $value['tax_inc_selling_price']. '' : '';
$pos_discount_price = $value['pos_discount_price']? $value['pos_discount_price']. '' : '';

//stockno
$stockno = $value['shape']. "-". $value['id'];
// price
if ($value["nego_status"] == '9') {
    $price = '<p class="souldout">ご成約</p>';
} elseif (is_numeric($value["app_price_any"])) {
    $price = '<span class="detil_price_number">'.$value["app_price_any"] . '</span><span class="detil_price_unit">万円[税込]</span>';
} else {
    $price = '<span class="ask">ASK</span>';
}
//inspect
$inspect = $inspect_any == "-"? $inspect_year. $inspect_month. $inspect_day: $inspect_any;
//freezing_test
$freezing_test = $freezing_inspecting_flag? '点検中': $freezing_before_operate_temperature. 'から'. $freezing_measurement_time. 'で'. $freezing_measurement_temprature. '確認済み';